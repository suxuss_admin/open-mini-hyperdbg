@echo off
echo Cleaning files ...
echo.

del    *.VC.db  /s
del    *.bsc    /s
:: del    *.pdb    /s   
del    *.iobj   /s
del    *.ipdb   /s
del    *.ilk    /s
del    *.ipch   /s
del    *.obj    /s
del    *.sbr    /s
del    *.tlog   /s
del    *.pch    /s

echo Done!
pause
