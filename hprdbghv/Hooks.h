/**
 * @file Hooks.h
 * @author Sina Karvandi (sina@rayanfam.com)
 * @brief Hook headers
 * @details 
 * @version 0.1
 * @date 2020-04-11
 * 
 * @copyright This project is released under the GNU Public License v3.
 * 
 */

#pragma once

//////////////////////////////////////////////////
//				   Syscall Hook					//
//////////////////////////////////////////////////

/**
 * @brief As we have just on sysret in all the Windows,
 * we use the following variable to hold its address
 * this way, we're not force to check for the instruction
 * so we remove the memory access to check for sysret 
 * in this case.
 * 
 */

/**
 * @brief 检查指令sysret和syscall
 * 
 */
// SYSRET在Windows内核中为SYSRETQ，长度为3，16进制为0X48,0X0F,0X07
#define IS_SYSRET_INSTRUCTION(Code)   \
    (*((PUINT8)(Code) + 0) == 0x48 && \
     *((PUINT8)(Code) + 1) == 0x0F && \
     *((PUINT8)(Code) + 2) == 0x07)
// 在Windows系统下，SYSCALL只会出现在应用层，长度为2，16进制为0x0F,0x05
#define IS_SYSCALL_INSTRUCTION(Code)  \
    (*((PUINT8)(Code) + 0) == 0x0F && \
     *((PUINT8)(Code) + 1) == 0x05)

#define IMAGE_DOS_SIGNATURE    0x5A4D     // MZ
#define IMAGE_OS2_SIGNATURE    0x454E     // NE
#define IMAGE_OS2_SIGNATURE_LE 0x454C     // LE
#define IMAGE_VXD_SIGNATURE    0x454C     // LE
#define IMAGE_NT_SIGNATURE     0x00004550 // PE00

//////////////////////////////////////////////////
//				   Structure					//
//////////////////////////////////////////////////

//内存隐写结构
typedef struct _HideMM_Buf
{
	PVOID Buffer;
	UINT32 BuffSize;
} HideMM_Buf, *PHideMM_Buf;

/**
 * @brief SSDT structure
 * 
 */
typedef struct _SSDTStruct
{
    LONG * pServiceTable;
    PVOID  pCounterTable;
#ifdef _WIN64
    ULONGLONG NumberOfServices;
#else
    ULONG NumberOfServices;
#endif
    PCHAR pArgumentTable;
} SSDTStruct, *PSSDTStruct;

typedef struct _HIDDEN_HOOKS_DETOUR_DETAILS
{
    LIST_ENTRY OtherHooksList;
    PVOID      HookedFunctionAddress;
    PVOID      ReturnAddress;
} HIDDEN_HOOKS_DETOUR_DETAILS, *PHIDDEN_HOOKS_DETOUR_DETAILS;

typedef struct _SYSTEM_MODULE_ENTRY
{
    HANDLE Section;
    PVOID  MappedBase;
    PVOID  ImageBase;
    ULONG  ImageSize;
    ULONG  Flags;
    USHORT LoadOrderIndex;
    USHORT InitOrderIndex;
    USHORT LoadCount;
    USHORT OffsetToFileName;
    UCHAR  FullPathName[256];
} SYSTEM_MODULE_ENTRY, *PSYSTEM_MODULE_ENTRY;

typedef struct _SYSTEM_MODULE_INFORMATION
{
    ULONG               Count;
    SYSTEM_MODULE_ENTRY Module[0];
} SYSTEM_MODULE_INFORMATION, *PSYSTEM_MODULE_INFORMATION;

typedef enum _SYSTEM_INFORMATION_CLASS
{
    SystemModuleInformation         = 11,
    SystemKernelDebuggerInformation = 35
} SYSTEM_INFORMATION_CLASS,
    *PSYSTEM_INFORMATION_CLASS;

typedef NTSTATUS(NTAPI * ZWQUERYSYSTEMINFORMATION)(
    IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
    OUT PVOID                   SystemInformation,
    IN ULONG                    SystemInformationLength,
    OUT PULONG ReturnLength     OPTIONAL);

NTSTATUS(*NtCreateFileOrig)
(

	_Out_ PHANDLE FileHandle,
	_In_ ACCESS_MASK DesiredAccess,
	_In_ POBJECT_ATTRIBUTES ObjectAttributes,
	_Out_ PIO_STATUS_BLOCK IoStatusBlock,
	_In_opt_ PLARGE_INTEGER AllocationSize,
	_In_ ULONG FileAttributes,
	_In_ ULONG ShareAccess,
	_In_ ULONG CreateDisposition,
	_In_ ULONG CreateOptions,
	_In_reads_bytes_opt_(EaLength) PVOID EaBuffer,
	_In_ ULONG EaLength
	);

NTSTATUS NtCreateFileHook(
	_Out_ PHANDLE FileHandle,
	_In_ ACCESS_MASK DesiredAccess,
	_In_ POBJECT_ATTRIBUTES ObjectAttributes,
	_Out_ PIO_STATUS_BLOCK IoStatusBlock,
	_In_opt_ PLARGE_INTEGER AllocationSize,
	_In_ ULONG FileAttributes,
	_In_ ULONG ShareAccess,
	_In_ ULONG CreateDisposition,
	_In_ ULONG CreateOptions,
	_In_reads_bytes_opt_(EaLength) PVOID EaBuffer,
	_In_ ULONG EaLength
);

NTSTATUS
(*NtOpenProcessOrig)(
	_Out_ PHANDLE ProcessHandle,
	_In_ ACCESS_MASK DesiredAccess,
	_In_ POBJECT_ATTRIBUTES ObjectAttributes,
	_In_opt_ PCLIENT_ID ClientId
);

NTSTATUS
NtOpenProcessHook(
	_Out_ PHANDLE ProcessHandle,
	_In_ ACCESS_MASK DesiredAccess,
	_In_ POBJECT_ATTRIBUTES ObjectAttributes,
	_In_opt_ PCLIENT_ID ClientId
);

NTSTATUS
(*ObReferenceObjectByHandleOrig)(
	_In_ HANDLE Handle,
	_In_ ACCESS_MASK DesiredAccess,
	_In_opt_ POBJECT_TYPE ObjectType,
	_In_ KPROCESSOR_MODE AccessMode,
	_Out_ PVOID *Object,
	_Out_opt_ POBJECT_HANDLE_INFORMATION HandleInformation
	);

NTSTATUS
ObReferenceObjectByHandleHook(
	_In_ HANDLE Handle,
	_In_ ACCESS_MASK DesiredAccess,
	_In_opt_ POBJECT_TYPE ObjectType,
	_In_ KPROCESSOR_MODE AccessMode,
	_Out_ PVOID *Object,
	_Out_opt_ POBJECT_HANDLE_INFORMATION HandleInformation
);

NTSTATUS
(*NtAllocateVirtualMemoryOrig)(
	_In_ HANDLE ProcessHandle,
	_Inout_ _At_(*BaseAddress, _Readable_bytes_(*RegionSize) _Writable_bytes_(*RegionSize) _Post_readable_byte_size_(*RegionSize)) PVOID *BaseAddress,
	_In_ ULONG_PTR ZeroBits,
	_Inout_ PSIZE_T RegionSize,
	_In_ ULONG AllocationType,
	_In_ ULONG Protect
);


NTSTATUS
NtAllocateVirtualMemoryHook(
	_In_ HANDLE ProcessHandle,
	_Inout_ _At_(*BaseAddress, _Readable_bytes_(*RegionSize) _Writable_bytes_(*RegionSize) _Post_readable_byte_size_(*RegionSize)) PVOID *BaseAddress,
	_In_ ULONG_PTR ZeroBits,
	_Inout_ PSIZE_T RegionSize,
	_In_ ULONG AllocationType,
	_In_ ULONG Protect
);

NTSTATUS
(*NtQueryVirtualMemoryOrig)(
	_In_ HANDLE ProcessHandle,
	_In_opt_ PVOID BaseAddress,
	_In_ MEMORY_INFORMATION_CLASS MemoryInformationClass,
	_Out_writes_bytes_(MemoryInformationLength) PVOID MemoryInformation,
	_In_ SIZE_T MemoryInformationLength,
	_Out_opt_ PSIZE_T ReturnLength
);

NTSTATUS
NtQueryVirtualMemoryHook(
	_In_ HANDLE ProcessHandle,
	_In_opt_ PVOID BaseAddress,
	_In_ MEMORY_INFORMATION_CLASS MemoryInformationClass,
	_Out_writes_bytes_(MemoryInformationLength) PVOID MemoryInformation,
	_In_ SIZE_T MemoryInformationLength,
	_Out_opt_ PSIZE_T ReturnLength
);

NTSTATUS(*NtUserFindWindowExOrig)(
	IN HWND hwndParent,
	IN HWND hwndChild,
	IN PUNICODE_STRING pstrClassName OPTIONAL,
	IN PUNICODE_STRING pstrWindowName OPTIONAL,
	IN DWORD dwType);

NTSTATUS
NtUserFindWindowExHook(
	IN HWND hwndParent,
	IN HWND hwndChild,
	IN PUNICODE_STRING pstrClassName OPTIONAL,
	IN PUNICODE_STRING pstrWindowName OPTIONAL,
	IN DWORD dwType);


NTSTATUS
(*TZwQuerySystemInformation)(
    _In_ SYSTEM_INFORMATION_CLASS SystemInformationClass,
    _Inout_ PVOID                 SystemInformation,
    _In_ ULONG                    SystemInformationLength,
    _Out_opt_ PULONG              ReturnLength);

PVOID ApiLocationFromSSDTOfNtCreateFile;
PVOID ApiLocationFromSSDTOfNtOpenProcess;
PVOID ApiLocationFromSSDTOfObReferenceObjectByHandle;

//////////////////////////////////////////////////
//		    	 Hidden Hooks Test				//
//////////////////////////////////////////////////

PVOID(*ExAllocatePoolWithTagOrig)
(
    POOL_TYPE PoolType,
    SIZE_T    NumberOfBytes,
    ULONG     Tag);

// ----------------------------------------------------------------------

/**
 * @brief Hook in VMX Root Mode with hidden detours and monitor
 * (A pre-allocated buffer should be available)
 * 
 * @param TargetAddress 
 * @param HookFunction 
 * @param ProcessCr3 
 * @param UnsetRead 
 * @param UnsetWrite 
 * @param UnsetExecute 
 * @return BOOLEAN 
 */
BOOLEAN
EptHookPerformPageHook2(PVOID TargetAddress, PVOID HookFunction, CR3_TYPE ProcessCr3, BOOLEAN UnsetRead, BOOLEAN UnsetWrite, BOOLEAN UnsetExecute);

/**
 * @brief Hook in VMX Non Root Mode (hidden detours)
 * 
 * @param TargetAddress 
 * @param HookFunction 
 * @param ProcessId 
 * @param SetHookForRead 
 * @param SetHookForWrite 
 * @param SetHookForExec 
 * @return BOOLEAN 
 */
PVOID
EptHook2(PVOID TargetAddress, PVOID HookFunction,UINT32 ProcessId, BOOLEAN eSetHookForRead, BOOLEAN eSetHookForWrite, BOOLEAN eSetHookForExec);

/**
 * @brief Handle hooked pages in Vmx-root mode
 * 
 * @param Regs 
 * @param HookedEntryDetails 
 * @param ViolationQualification 
 * @param PhysicalAddress 
 * @return BOOLEAN 
 */
BOOLEAN
EptHookHandleHookedPage(PGUEST_REGS Regs, EPT_HOOKED_PAGE_DETAIL * HookedEntryDetails, VMX_EXIT_QUALIFICATION_EPT_VIOLATION ViolationQualification, SIZE_T PhysicalAddress);

/**
 * @brief Knernel Api Hook
 *
 * @param HookFunction 钩子触发时将调用的函数
 * @param ApiName 要HOOK的内核导出函数
 * @return 成功返回跳板地址，失败则返回Null
 */
PVOID
EptHookApi(PVOID HookFunction, PCWSTR ApiName);

PVOID
EptHookApiInPid(PVOID HookFunction, PCWSTR ApiName, UINT32 Pid);

PVOID
EptHookApiByPoint(PVOID HookFunction, PVOID ApiFun);
/**
 * @brief Remove a special hook from the hooked pages lists
 * 
 * @param PhysicalAddress 
 * @return BOOLEAN 
 */
BOOLEAN
EptHookRestoreSingleHookToOrginalEntry(SIZE_T PhysicalAddress);

/**
 * @brief Remove all hooks from the hooked pages lists (Should be called in vmx-root)
 * 
 * @return VOID 
 */
VOID
EptHookRestoreAllHooksToOrginalEntry();

/**
 * @brief Remove all hooks from the hooked pages lists
 * 
 * @return VOID 
 */
VOID
EptHookUnHookAll();

VOID
EptRemoveHookAndInvalidateAllEntries();

/**
 * @brief Remove single hook from the hooked pages list and invalidate TLB
 * 
 * @param VirtualAddress 
 * @param ProcessId 
 * @return BOOLEAN 
 */
BOOLEAN
EptHookUnHookSingleAddress(UINT64 VirtualAddress, UINT32 ProcessId);

/**
 * @brief Remove an entry from g_EptHook2sDetourListHead
 * 
 * @param Address 
 * @return BOOLEAN 
 */
BOOLEAN
EptHookRemoveEntryAndFreePoolFromEptHook2sDetourList(UINT64 Address);


/**
 * @brief Return the springboard function of hook
 * By:Lonely
 * @param Address
 * @return PVOID
 */
PVOID
EptHookResultTrampoline(UINT64 VirtualAddress);


VOID
HvEptHookFlushAndInvalidateTLB();

VOID
EptHookFlushAndInvalidateTLB();

