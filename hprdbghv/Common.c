/**
 * @file Common.c
 * @author Sina Karvandi (sina@rayanfam.com)
 * @brief Common functions that needs to be used in all source code files
 * @details
 * @version 0.1
 * @date 2020-04-10
 * 
 * @copyright This project is released under the GNU Public License v3.
 * 
 */
#include "pch.h"

 /**
  * @brief 原始的AsmVmxVmcall
  */
NTSTATUS AsmVmxVmcall(unsigned long long VmcallNumber, unsigned long long OptionalParam1, unsigned long long OptionalParam2, long long OptionalParam3)
{
	return AsmVmxVmcallEx(VmcallNumber, 3, OptionalParam1, OptionalParam2, OptionalParam3);
}

/**
 * @brief 增强的AsmVmxVmcall
 * @param VmcallNumber
 * @param 参数数量
 * @param 参数...
 */
NTSTATUS AsmVmxVmcallEx(unsigned long long VmcallNumber, unsigned long long num, ...)
{
	unsigned long long arg_ptr = 0;

	//初始化不定长参数指针
	va_start(arg_ptr, num);

	//申请内存
    PVM_CALL_OptionalParams OptionalParams = ExAllocatePoolWithTag(NonPagedPool, sizeof(unsigned long long) * num, POOLTAG);

	//清零内存
	RtlZeroMemory(OptionalParams, sizeof(VM_CALL_OptionalParams) * num);
	
	//遍历不定长参数
	for (int i = 0; i < num; i++)
	{
		OptionalParams->OptionalParams[i] = va_arg(arg_ptr, unsigned long long);
	}

	va_end(arg_ptr);

	NTSTATUS status = asm_AsmVmxVmcall(VmcallNumber, OptionalParams, num, 0);

	ExFreePoolWithTag(OptionalParams, POOLTAG);

    return status;
}

/**
 * @brief Power function in order to computer address for MSR bitmaps
 *
 * @param Base Base for power function
 * @param Exp Exponent for power function
 * @return int Returns the result of power function
 */
int
MathPower(int Base, int Exp)
{
    int result;

    result = 1;

    for (;;)
    {
        if (Exp & 1)
        {
            result *= Base;
        }
        Exp >>= 1;
        if (!Exp)
        {
            break;
        }
        Base *= Base;
    }
    return result;
}

/**
 * @brief 向所有逻辑核心广播一个函数
 * @details 此函数已弃用，因为我们希望支持32个以上的处理器,请使用DPC
 * 
 * @param ProcessorNumber The logical core number to execute routine on it
 * @param Routine The fucntion that should be executed on the target core
 * @return BOOLEAN Returns true if it was successfull
 */
BOOLEAN
BroadcastToProcessors(ULONG ProcessorNumber, RunOnLogicalCoreFunc Routine)
{
    KIRQL OldIrql;

    KeSetSystemAffinityThread((KAFFINITY)(1 << ProcessorNumber));

    OldIrql = KeRaiseIrqlToDpcLevel();

    Routine(ProcessorNumber);

    KeLowerIrql(OldIrql);

    KeRevertToUserAffinityThread();

    return TRUE;
}

/**
 * @brief 检查该位是否设置
 * 
 * @param nth 
 * @param addr 
 * @return int 
 */
int
TestBit(int nth, unsigned long * addr)
{
    return (BITMAP_ENTRY(nth, addr) >> BITMAP_SHIFT(nth)) & 1;
}

/**
 * @brief unset the bit
 * 
 * @param nth 
 * @param addr 
 */
void
ClearBit(int nth, unsigned long * addr)
{
    BITMAP_ENTRY(nth, addr) &= ~(1UL << BITMAP_SHIFT(nth));
}

/**
 * @brief set the bit
 * 
 * @param nth 
 * @param addr 
 */
void
SetBit(int nth, unsigned long * addr)
{
    BITMAP_ENTRY(nth, addr) |= (1UL << BITMAP_SHIFT(nth));
}

/**
 * @brief 将虚拟地址转换为物理地址
 * 
 * @param VirtualAddress The target virtual address
 * @return UINT64 Returns the physical address
 */
UINT64
VirtualAddressToPhysicalAddress(PVOID VirtualAddress)
{
    return MmGetPhysicalAddress(VirtualAddress).QuadPart;
}

/**
 * @brief 获取进程的CR3
 * 
 * @details this function should NOT be called from vmx-root mode
 *
 * @param ProcessId ProcessId to switch
 * @return CR3_TYPE The cr3 of the target process 
 */
CR3_TYPE
GetCr3FromProcessId(UINT32 ProcessId)
{
    PEPROCESS TargetEprocess;
    CR3_TYPE  ProcessCr3 = {0};

    if (PsLookupProcessByProcessId(ProcessId, &TargetEprocess) != STATUS_SUCCESS)
    {
        //
        // There was an error, probably the process id was not found
        //
        return ProcessCr3;
    }

    //
    // Due to KVA Shadowing, we need to switch to a different directory table base
    // if the PCID indicates this is a user mode directory table base.
    //
    NT_KPROCESS * CurrentProcess = (NT_KPROCESS *)(TargetEprocess);
    ProcessCr3.Flags             = CurrentProcess->DirectoryTableBase;

    return ProcessCr3;
}

/**
 * @brief 切换到另一个进程的cr3
 * 
 * @details this function should NOT be called from vmx-root mode
 *
 * @param ProcessId ProcessId to switch
 * @return CR3_TYPE The cr3 of current process which can be
 * used by RestoreToPreviousProcess function
 */
CR3_TYPE
SwitchOnAnotherProcessMemoryLayout(UINT32 ProcessId)
{
    UINT64    GuestCr3;
    PEPROCESS TargetEprocess;
    CR3_TYPE  CurrentProcessCr3 = {0};

    if (PsLookupProcessByProcessId(ProcessId, &TargetEprocess) != STATUS_SUCCESS)
    {
        //
        // There was an error, probably the process id was not found
        //
        return CurrentProcessCr3;
    }

    //
    // Due to KVA Shadowing, we need to switch to a different directory table base
    // if the PCID indicates this is a user mode directory table base.
    //
    NT_KPROCESS * CurrentProcess = (NT_KPROCESS *)(TargetEprocess);
    GuestCr3                     = CurrentProcess->DirectoryTableBase;

    //
    // Read the current cr3
    //
    CurrentProcessCr3.Flags = __readcr3();

    //
    // Change to a new cr3 (of target process)
    //
    __writecr3(GuestCr3);

    return CurrentProcessCr3;
}

/**
 * @brief Switch to another process's cr3
 * 切换到指定进程Cr3
 * 
 * @param TargetCr3 cr3 to switch
 * @return CR3_TYPE The cr3 of current process which can be
 * used by RestoreToPreviousProcess function
 */
CR3_TYPE
SwitchOnAnotherProcessMemoryLayoutByCr3(CR3_TYPE TargetCr3)
{
//    PEPROCESS TargetEprocess;
    CR3_TYPE  CurrentProcessCr3 = {0};

    //
    // Read the current cr3
    //
    CurrentProcessCr3.Flags = __readcr3();

    //
    // Change to a new cr3 (of target process)
    //
    __writecr3(TargetCr3.Flags);

    return CurrentProcessCr3;
}

/**
 * @brief 获取段描述符
 * 
 * @param SegmentSelector 
 * @param Selector 
 * @param GdtBase 
 * @return BOOLEAN 
 */
BOOLEAN
GetSegmentDescriptor(PSEGMENT_SELECTOR SegmentSelector, USHORT Selector, PUCHAR GdtBase)
{
    PSEGMENT_DESCRIPTOR SegDesc;

    if (!SegmentSelector)
        return FALSE;

    if (Selector & 0x4)
    {
        return FALSE;
    }

    SegDesc = (PSEGMENT_DESCRIPTOR)((PUCHAR)GdtBase + (Selector & ~0x7));

    SegmentSelector->SEL               = Selector;
    SegmentSelector->BASE              = SegDesc->BASE0 | SegDesc->BASE1 << 16 | SegDesc->BASE2 << 24;
    SegmentSelector->LIMIT             = SegDesc->LIMIT0 | (SegDesc->LIMIT1ATTR1 & 0xf) << 16;
    SegmentSelector->ATTRIBUTES.UCHARs = SegDesc->ATTR0 | (SegDesc->LIMIT1ATTR1 & 0xf0) << 4;

    if (!(SegDesc->ATTR0 & 0x10))
    {
        //
        // LA_ACCESSED
        //
        ULONG64 tmp;

        //
        // this is a TSS or callgate etc, save the base high part
        //
        tmp                   = (*(PULONG64)((PUCHAR)SegDesc + 8));
        SegmentSelector->BASE = (SegmentSelector->BASE & 0xffffffff) | (tmp << 32);
    }

    if (SegmentSelector->ATTRIBUTES.Fields.G)
    {
        //
        // 4096-bit granularity is enabled for this segment, scale the limit
        //
        SegmentSelector->LIMIT = (SegmentSelector->LIMIT << 12) + 0xfff;
    }

    return TRUE;
}

/**
 * @brief Switch to previous process's cr3
 * 恢复CR3
 * 
 * @param PreviousProcess Cr3 of previous process which 
 * is returned by SwitchOnAnotherProcessMemoryLayout
 * @return VOID 
 */
VOID
RestoreToPreviousProcess(CR3_TYPE PreviousProcess)
{
    //
    // Restore the original cr3
    //
    __writecr3(PreviousProcess.Flags);
}

/**
 * @brief 根据特定进程id将物理地址转换为虚拟地址
 *   
 * @details this function should NOT be called from vmx-root mode
 *
 * @param PhysicalAddress The target physical address
 * @param ProcessId The target's process id
 * @return UINT64 Returns the virtual address
 */
UINT64
PhysicalAddressToVirtualAddressByProcessId(PVOID PhysicalAddress, UINT32 ProcessId)
{
    CR3_TYPE         CurrentProcessCr3;
    UINT64           VirtualAddress;
    PHYSICAL_ADDRESS PhysicalAddr;

    //
    // Switch to new process's memory layout
    //
    CurrentProcessCr3 = SwitchOnAnotherProcessMemoryLayout(ProcessId);

    //
    // Validate if process id is valid
    //
    if (CurrentProcessCr3.Flags == NULL)
    {
        //
        // Pid is invalid
        //
        return NULL;
    }

    //
    // Read the virtual address based on new cr3
    //
    PhysicalAddr.QuadPart = PhysicalAddress;
    VirtualAddress        = MmGetVirtualForPhysical(PhysicalAddr);

    //
    // Restore the original process
    //
    RestoreToPreviousProcess(CurrentProcessCr3);

    return VirtualAddress;
}

/**
 * @brief 基于特定进程的内核cr3将物理地址转换为虚拟地址
 *   
 * @details this function should NOT be called from vmx-root mode
 *
 * @param PhysicalAddress The target physical address
 * @param TargetCr3 The target's process cr3
 * @return UINT64 Returns the virtual address
 */
UINT64
PhysicalAddressToVirtualAddressByCr3(PVOID PhysicalAddress, CR3_TYPE TargetCr3)
{
    CR3_TYPE         CurrentProcessCr3;
    UINT64           VirtualAddress;
    PHYSICAL_ADDRESS PhysicalAddr;

    //
    // Switch to new process's memory layout
    //
    CurrentProcessCr3 = SwitchOnAnotherProcessMemoryLayoutByCr3(TargetCr3);

    //
    // Validate if process id is valid
    //
    if (CurrentProcessCr3.Flags == NULL)
    {
        //
        // Pid is invalid
        //
        return NULL;
    }

    //
    // Read the virtual address based on new cr3
    //
    PhysicalAddr.QuadPart = PhysicalAddress;
    VirtualAddress        = MmGetVirtualForPhysical(PhysicalAddr);

    //
    // Restore the original process
    //
    RestoreToPreviousProcess(CurrentProcessCr3);

    return VirtualAddress;
}

/**
 * @brief 基于特定进程id的内核cr3将虚拟地址转换为物理地址
 *
 * @details this function should NOT be called from vmx-root mode
 * 
 * @param VirtualAddress The target virtual address
 * @param ProcessId The target's process id
 * @return UINT64 Returns the physical address
 */
UINT64
VirtualAddressToPhysicalAddressByProcessId(PVOID VirtualAddress, UINT32 ProcessId)
{
    CR3_TYPE CurrentProcessCr3;
    UINT64   PhysicalAddress;

    //
    // Switch to new process's memory layout
    //
    CurrentProcessCr3 = SwitchOnAnotherProcessMemoryLayout(ProcessId);

    //
    // Validate if process id is valid
    //
    if (CurrentProcessCr3.Flags == NULL)
    {
        //
        // Pid is invalid
        //
        return NULL;
    }

    //
    // Read the physical address based on new cr3
    //
    PhysicalAddress = MmGetPhysicalAddress(VirtualAddress).QuadPart;

    //
    // Restore the original process
    //
    RestoreToPreviousProcess(CurrentProcessCr3);

    return PhysicalAddress;
}

/**
 * @brief 基于特定进程内核CR3将虚拟地址转换为物理地址
 *
 * @details this function should NOT be called from vmx-root mode
 * 
 * @param VirtualAddress The target virtual address
 * @param TargetCr3 The target's process cr3
 * @return UINT64 Returns the physical address
 */
UINT64
VirtualAddressToPhysicalAddressByProcessCr3(PVOID VirtualAddress, CR3_TYPE TargetCr3)
{
    CR3_TYPE CurrentProcessCr3;
    UINT64   PhysicalAddress;

    //
    // Switch to new process's memory layout
    // 切换CR3
    CurrentProcessCr3 = SwitchOnAnotherProcessMemoryLayoutByCr3(TargetCr3);

    //
    // Validate if process id is valid
    //
    if (CurrentProcessCr3.Flags == NULL)
    {
        //
        // Pid is invalid
        //
        return NULL;
    }

    //
    // Read the physical address based on new cr3
    // 使用winapi计算物理地址
    PhysicalAddress = MmGetPhysicalAddress(VirtualAddress).QuadPart;

    //
    // Restore the original process
    // 恢复CR3
    RestoreToPreviousProcess(CurrentProcessCr3);

    return PhysicalAddress;
}

/**
 * @brief 将物理地址转换为虚拟地址
 * 
 * @param PhysicalAddress The target physical address
 * @return UINT64 Returns the virtual address
 */
UINT64
PhysicalAddressToVirtualAddress(UINT64 PhysicalAddress)
{
    PHYSICAL_ADDRESS PhysicalAddr;
    PhysicalAddr.QuadPart = PhysicalAddress;

    return MmGetVirtualForPhysical(PhysicalAddr);
}

/**
 * @brief 查找system的Cr3
 * 
 * @return UINT64 Returns cr3 of System process (pid=4)
 */
UINT64
FindSystemDirectoryTableBase()
{
    //
    // Return CR3 of the system process.
    //
    NT_KPROCESS * SystemProcess = (NT_KPROCESS *)(PsInitialSystemProcess);
    return SystemProcess->DirectoryTableBase;
}

/**
 * @brief EPROCESS获取进程名
 * 
 * @param Eprocess Process eprocess
 * @return PCHAR Returns a pointer to the process name
 */
PCHAR
GetProcessNameFromEprocess(PEPROCESS Eprocess)
{
    PCHAR Result = 0;

    //
    // We can't use PsLookupProcessByProcessId as in pageable and not
    // work on vmx-root
    //
    Result = (CHAR *)PsGetProcessImageFileName(Eprocess);

    return Result;
}

/**
 * @brief 检测字符串是否以另一个字符串开头
 * 
 * @param const char * pre
 * @param const char * str
 * @return BOOLEAN Returns true if it starts with and false if not strats with
 */
BOOLEAN
StartsWith(const char * pre, const char * str)
{
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? FALSE : memcmp(pre, str, lenpre) == 0;
}

/**
 * @brief 检查ProcId为的进程是否存在
 * 
 * @details this function should NOT be called from vmx-root mode
 *
 * @param UINT32 ProcId
 * @return BOOLEAN Returns true if the process 
 * exists and false if it the process doesn't exist
 */
BOOLEAN
IsProcessExist(UINT32 ProcId)
{
    PEPROCESS TargetEprocess;
//    CR3_TYPE  CurrentProcessCr3 = {0};

    if (PsLookupProcessByProcessId(ProcId, &TargetEprocess) != STATUS_SUCCESS)
    {
        //
        // There was an error, probably the process id was not found
        //
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}


/* 获取系统版本 */
WIN_VER_DETAIL GetWindowsVersion()
{
	RTL_OSVERSIONINFOEXW    osverinfo;
	WIN_VER_DETAIL WinVersion = WINDOWS_VERSION_NONE;

	memset(&osverinfo, 0, sizeof(RTL_OSVERSIONINFOEXW));
	osverinfo.dwOSVersionInfoSize = sizeof(RTL_OSVERSIONINFOEXW);

	if (RtlGetVersion((RTL_OSVERSIONINFOW*)&osverinfo) != STATUS_SUCCESS) {
		return WINDOWS_VERSION_NONE;
	}
	else if (osverinfo.dwMajorVersion == 6 && osverinfo.dwMinorVersion == 1 && osverinfo.dwBuildNumber == 7000) {
		WinVersion = WINDOWS_VERSION_7_7000;
	}
	else if (osverinfo.dwMajorVersion == 6 && osverinfo.dwMinorVersion == 1 && osverinfo.dwBuildNumber >= 7600) {
		WinVersion = WINDOWS_VERSION_7_7600_UP;
	}
	else if (osverinfo.dwMajorVersion == 10 && osverinfo.dwMinorVersion == 0 && osverinfo.dwBuildNumber == 15063) {
		WinVersion = WINDOWS_VERSION_10_15063;
	}
	else if (osverinfo.dwMajorVersion == 10 && osverinfo.dwMinorVersion == 0 && osverinfo.dwBuildNumber == 16299) {
		WinVersion = WINDOWS_VERSION_10_16299;
	}
	else if (osverinfo.dwMajorVersion == 10 && osverinfo.dwMinorVersion == 0 && osverinfo.dwBuildNumber == 17134) {
		WinVersion = WINDOWS_VERSION_10_17134;
	}
	else if (osverinfo.dwMajorVersion == 10 && osverinfo.dwMinorVersion == 0 && osverinfo.dwBuildNumber == 17763) {
		WinVersion = WINDOWS_VERSION_10_17763;
	}
	else if (osverinfo.dwMajorVersion == 10 && osverinfo.dwMinorVersion == 0 && osverinfo.dwBuildNumber == 18362) {
		WinVersion = WINDOWS_VERSION_10_18362;
	}
	else if (osverinfo.dwMajorVersion == 10 && osverinfo.dwMinorVersion == 0 && osverinfo.dwBuildNumber == 18363) {
		WinVersion = WINDOWS_VERSION_10_18363;
	}
	else if (osverinfo.dwMajorVersion == 10 && osverinfo.dwMinorVersion == 0 && osverinfo.dwBuildNumber >= 19041) {
		WinVersion = WINDOWS_VERSION_10_19041_UP;
	}
	LogInfo("WinVersion: dwMajorVersion=%d; dwMinorVersion=%d; dwBuildNumber=%d;\n", osverinfo.dwMajorVersion, osverinfo.dwMinorVersion, osverinfo.dwBuildNumber);
	return WinVersion;
}