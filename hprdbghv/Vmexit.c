/**
 * @file Vmexit.c
 * @author Sina Karvandi (sina@rayanfam.com)
 * @brief The functions for VM-Exit handler for different exit reasons 
 * @details
 * @version 0.1
 * @date 2020-04-11
 * 
 * @copyright This project is released under the GNU Public License v3.
 * 
 */
#include "pch.h"

/**
 * @brief VM-Exit handler for different exit reasons
 * 27.2.1 Basic VM-Exit Information
 * @param GuestRegs Registers that are automatically saved by AsmVmexitHandler (HOST_RIP)
 * @return BOOLEAN Return True if VMXOFF executed (not in vmx anymore),
 * or return false if we are still in vmx (so we should use vm resume)
 * This field is saved for VM exits due to the following causes: debug exceptions; page-fault
 * exceptions; start-up IPIs (SIPIs); system-management interrupts (SMIs) that arrive immediately after the
 * execution of I/O instructions; task switches; INVEPT; INVLPG; INVPCID; INVVPID; LGDT; LIDT; LLDT; LTR;
 * SGDT; SIDT; SLDT; STR; VMCLEAR; VMPTRLD; VMPTRST; VMREAD; VMWRITE; VMXON; XRSTORS; XSAVES;
 * control-register accesses; MOV DR; I/O instructions; MWAIT; accesses to the APIC-access page (see Section
 * 29.4); EPT violations (see Section 28.2.3.2); EOI virtualization (see Section 29.1.4); APIC-write emulation
 * (see Section 29.4.3.3); page-modification log full (see Section 28.2.6); and SPP-related events (see Section
 * 28.2.4). For all other VM exits, this field is cleared. The following items provide details:
 * Vm-Exit核心处理
 */
BOOLEAN
VmxVmexitHandler(PGUEST_REGS GuestRegs)
{
    VMEXIT_INTERRUPT_INFO InterruptExit         = {0};
    IO_EXIT_QUALIFICATION IoQualification       = {0};
    RFLAGS                Flags                 = {0};
    UINT64                GuestPhysicalAddr     = 0;
    UINT64                GuestRsp              = 0;
    ULONG                 ExitReason            = 0;
    ULONG                 ExitQualification     = 0;
    ULONG                 Rflags                = 0;
    ULONG                 EcxReg                = 0;
//    ULONG                 ExitInstructionLength = 0;
    ULONG                 CurrentProcessorIndex = 0;
    BOOLEAN               Result                = FALSE;
    BOOLEAN               ShouldEmulateRdtscp   = TRUE;

    //
    // *********** SEND MESSAGE AFTER WE SET THE STATE ***********
    //
    CurrentProcessorIndex = KeGetCurrentProcessorNumber();

    //
    // 表示我们在此逻辑核心中处于Vmx根模式
    //
    g_GuestState[CurrentProcessorIndex].IsOnVmxRootMode = TRUE;

    //
    // 读取退出代码
    //

    __vmx_vmread(VM_EXIT_REASON, &ExitReason);
    ExitReason &= 0xffff;

	if (g_TransparentMode)
	{
		ShouldEmulateRdtscp = TransparentModeStart(GuestRegs, CurrentProcessorIndex, ExitReason);

	}


    // Increase the RIP by default
    //
    g_GuestState[CurrentProcessorIndex].IncrementRip = TRUE;

    //
    // Set the rsp in general purpose registers structure
    //
    __vmx_vmread(GUEST_RSP, &GuestRsp);
    GuestRegs->rsp = GuestRsp;

    //
    // 阅读出口资格
    //

    __vmx_vmread(EXIT_QUALIFICATION, &ExitQualification);

    //
    // Debugging purpose
    //
    // LogInfo("VM_EXIT_REASON : 0x%x", ExitReason);
    // LogInfo("EXIT_QUALIFICATION : 0x%llx", ExitQualification);
    //

    switch (ExitReason)
    {
	case EXIT_REASON_VMFUNC:
	{
		if (GuestRegs->rax < 63)
		{
			LogError("call VM_FUNC.\n");
		}
		//直接注入UD异常
		EventInjectUndefinedOpcode(CurrentProcessorIndex);
		break;
	}
    case EXIT_REASON_TRIPLE_FAULT:
    {
        LogError("Triple fault error occured.\n");

        break;
    }
        //
        // 25.1.2  Instructions That Cause VM Exits Unconditionally
        // The following instructions cause VM exits when they are executed in VMX non-root operation: CPUID, GETSEC,
        // INVD, and XSETBV. This is also true of instructions introduced with VMX, which include: INVEPT, INVVPID,
        // VMCALL, VMCLEAR, VMLAUNCH, VMPTRLD, VMPTRST, VMRESUME, VMXOFF, and VMXON.
        //

    case EXIT_REASON_VMCLEAR:
    case EXIT_REASON_VMPTRLD:
    case EXIT_REASON_VMPTRST:
    case EXIT_REASON_VMREAD:
    case EXIT_REASON_VMRESUME:
    case EXIT_REASON_VMWRITE:
    case EXIT_REASON_VMXOFF:
    {
        //
        // cf=1 指示vm指令失败
        //
        __vmx_vmread(GUEST_RFLAGS, &Rflags);
        __vmx_vmwrite(GUEST_RFLAGS, Rflags | 0x1);

        //
        // 处理无条件的虚拟机退出 (inject #ud)
        // Ps:直接注入UD是不是过分了点，这样会蓝屏的兄弟
        //EventInjectUndefinedOpcode(CurrentProcessorIndex);
        //

        break;
    }
    case EXIT_REASON_VMXON:
    {
        //根据指令手册，BIOS中关闭VT的话，VMXON将返回GP
        EventInjectGeneralProtection();
        break;
    }
    case EXIT_REASON_VMLAUNCH:
    {
        //
        // cf=1 指示vm指令失败
        //
        __vmx_vmread(GUEST_RFLAGS, &Rflags);
        __vmx_vmwrite(GUEST_RFLAGS, Rflags | 0x1);

        //
        // 处理无条件的虚拟机退出 (inject #ud)
		// Ps:直接注入UD是不是过分了点，这样会蓝屏的兄弟
		//EventInjectUndefinedOpcode(CurrentProcessorIndex);
        //


        break;
    }
    case EXIT_REASON_INVEPT:
    case EXIT_REASON_INVVPID:
    case EXIT_REASON_GETSEC:
    case EXIT_REASON_INVD:
    {
        //
        // 处理无条件的虚拟机退出 (inject #ud)
		EventInjectUndefinedOpcode(CurrentProcessorIndex);
        //
        break;
    }
    case EXIT_REASON_CR_ACCESS:
    {
		//CR寄存器操作
        HvHandleControlRegisterAccess(GuestRegs, CurrentProcessorIndex);
        break;
    }
    case EXIT_REASON_MSR_READ:
    {
        EcxReg = GuestRegs->rcx & 0xffffffff;
        HvHandleMsrRead(GuestRegs);

        break;
    }
    case EXIT_REASON_MSR_WRITE:
    {
        EcxReg = GuestRegs->rcx & 0xffffffff;
        HvHandleMsrWrite(GuestRegs);

        break;
    }
    case EXIT_REASON_CPUID:
    {
        HvHandleCpuid(GuestRegs);
        break;
    }

    case EXIT_REASON_IO_INSTRUCTION:
    {
        //
        // Read the I/O Qualification which indicates the I/O instruction
        //
        __vmx_vmread(EXIT_QUALIFICATION, &IoQualification);

        //
        // Read Guest's RFLAGS
        //
        __vmx_vmread(GUEST_RFLAGS, &Flags);

        //
        // Call the I/O Handler
        //
        IoHandleIoVmExits(GuestRegs, IoQualification, Flags);

        break;
    }
    case EXIT_REASON_EPT_VIOLATION:
    {
        //
        // Reading guest physical address
        //
        //LogError("EXIT_REASON_EPT_VIOLATION");
        __vmx_vmread(GUEST_PHYSICAL_ADDRESS, &GuestPhysicalAddr);
		// EPT页的处理流程
		// EptHandleEptViolation -》 EptHandlePageHookExit
        if (EptHandleEptViolation(GuestRegs, ExitQualification, GuestPhysicalAddr) == FALSE)
        {
            LogError("There were errors in handling Ept Violation\n");
        }
        break;
    }
    case EXIT_REASON_EPT_MISCONFIG:
    {
        __vmx_vmread(GUEST_PHYSICAL_ADDRESS, &GuestPhysicalAddr);

        EptHandleMisconfiguration(GuestPhysicalAddr);

        break;
    }
    case EXIT_REASON_VMCALL:
    {
        //
        // Handle vm-exits of VMCALLs
        //
        VmxHandleVmcallVmExit(GuestRegs);

        break;
    }
    case EXIT_REASON_EXCEPTION_NMI:
    {
        //
        // read the exit reason
        //
        __vmx_vmread(VM_EXIT_INTR_INFO, &InterruptExit);

        //
        // Call the Exception Bitmap and NMI Handler
        //
        IdtEmulationHandleExceptionAndNmi(InterruptExit, CurrentProcessorIndex, GuestRegs);

        break;
    }
    case EXIT_REASON_EXTERNAL_INTERRUPT:
    {
        //
        // read the exit reason (for interrupt)
        //
        __vmx_vmread(VM_EXIT_INTR_INFO, &InterruptExit);

        //
        // Call External Interrupt Handler
        //
        IdtEmulationHandleExternalInterrupt(InterruptExit, CurrentProcessorIndex);


        break;
    }
    case EXIT_REASON_PENDING_VIRT_INTR:
    {
        //
        // Call the interrupt-window exiting handler to re-inject the previous
        // interrupts or disable the interrupt-window exiting bit
        //
        IdtEmulationHandleInterruptWindowExiting(CurrentProcessorIndex);

        break;
    }
    case EXIT_REASON_MONITOR_TRAP_FLAG:
    {
		ULONG64 CurrentRIP = 0;
		__vmx_vmread(GUEST_RIP, &CurrentRIP);
		//LogInfo("GUEST_RIP : 0x%llx", CurrentRIP);
        //已经读取完了，所以要换回假页面
        //
        // Monitor Trap Flag
        //
        if (g_GuestState[CurrentProcessorIndex].MtfEptHookRestorePoint)
        {
            //
            // Restore the previous state
            // 换回假页面
            //
            EptHandleMonitorTrapFlag(g_GuestState[CurrentProcessorIndex].MtfEptHookRestorePoint);

            //
            // Set it to NULL
            //
            g_GuestState[CurrentProcessorIndex].MtfEptHookRestorePoint = NULL;

			//
			// We don't need MTF anymore if it set to disable MTF
			// 关闭MTF单步，页已经被偷偷换掉了
			//
			HvSetMonitorTrapFlag(FALSE);
        }
        else
        {
            LogError("Why MTF occured ?!\n");
        }
        //
        // Redo the instruction
        // 重做指令
        //
        g_GuestState[CurrentProcessorIndex].IncrementRip = FALSE;



        break;
    }
    case EXIT_REASON_HLT:
    {
        //
        // We don't wanna halt
        //

        //
        //__halt();
        //
        break;
    }
    case EXIT_REASON_RDTSC:
    {
        //
        // Check whether we are allowed to change
        // the registers and emulate rdtsc or not
        //
        if (ShouldEmulateRdtscp)
        {
            //
            // handle rdtsc (emulate rdtsc)
            //
            CounterEmulateRdtsc(GuestRegs);

        }
        break;
    }
    case EXIT_REASON_RDTSCP:
    {
        //
        // Check whether we are allowed to change
        // the registers and emulate rdtscp or not
        //
        if (ShouldEmulateRdtscp)
        {
            //
            // handle rdtscp (emulate rdtscp)
            //
            CounterEmulateRdtscp(GuestRegs);

        }

        break;
    }
    case EXIT_REASON_RDPMC:
    {
        //
        // handle rdpmc (emulate rdpmc)
        //
        CounterEmulateRdpmc(GuestRegs);

        break;
    }
    case EXIT_REASON_DR_ACCESS:
    {
        //
        // Handle access to debug registers
        //
        HvHandleMovDebugRegister(CurrentProcessorIndex, GuestRegs);


        break;
    }
    case EXIT_REASON_XSETBV:
    {
        //
        // Handle xsetbv (unconditional vm-exit)
        //
        EcxReg = GuestRegs->rcx & 0xffffffff;
        VmxHandleXsetbv(EcxReg, GuestRegs->rdx << 32 | GuestRegs->rax);

        break;
    }
    default:
    {
        LogError("Unkown Vmexit, reason : 0x%llx\n", ExitReason);
        break;
    }
    }

    //
    // Check whether we need to increment the guest's ip or not
    // Also, we should not increment rip if a vmxoff executed
    //

    // 如果执行VMXOFF或者IncrementRip为True则跳过当前指令
    if (!g_GuestState[CurrentProcessorIndex].VmxoffState.IsVmxoffExecuted && g_GuestState[CurrentProcessorIndex].IncrementRip)
    {
        HvResumeToNextInstruction();
    }

	if (g_TransparentMode)
	{
		if (ExitReason != EXIT_REASON_RDTSC && ExitReason != EXIT_REASON_RDTSCP && ExitReason != EXIT_REASON_CPUID)
		{
			//
			// We not wanna change the global timer while RDTSC and RDTSCP
			// was the reason of vm-exit
			//
            //int Aux = 0;
            //
            //__writemsr(MSR_IA32_TIME_STAMP_COUNTER, __rdtscp(&Aux));
			__writemsr(MSR_IA32_TIME_STAMP_COUNTER, g_GuestState[CurrentProcessorIndex].TransparencyState.PreviousTimeStampCounter);
            
            
		}
	}
    //
    // Set indicator of Vmx non root mode to false
    //
    g_GuestState[CurrentProcessorIndex].IsOnVmxRootMode = FALSE;

    if (g_GuestState[CurrentProcessorIndex].VmxoffState.IsVmxoffExecuted)
        Result = TRUE;

    //
    // By default it's FALSE, if we want to exit vmx then it's TRUE
    //
    return Result;
}
