/**
 * @file EptHook.c
 * @author Sina Karvandi (sina@rayanfam.com)
 * @brief Implementation of different EPT hidden hooks functions
 * @details All the R/W hooks, Execute hooks and hardware register simulators
 * are implemented here
 *  
 * @version 0.1
 * @date 2020-04-11
 * 
 * @copyright This project is released under the GNU Public License v3.
 * 
 */
#include "pch.h"

/**
 * @brief Hook function that HooksExAllocatePoolWithTag
 * 
 * @param PoolType 
 * @param NumberOfBytes 
 * @param Tag 
 * @return PVOID 
 */
PVOID
ExAllocatePoolWithTagHook(
    POOL_TYPE PoolType,
    SIZE_T    NumberOfBytes,
    ULONG     Tag)
{
    LogInfo("ExAllocatePoolWithTag Called with : Tag = 0x%x , Number Of Bytes = %d , Pool Type = %d \n", Tag, NumberOfBytes, PoolType);
    return ExAllocatePoolWithTagOrig(PoolType, NumberOfBytes, Tag);
}

/**
 * @brief Remove and Invalidate Hook in TLB (Hidden Detours and if counter of hidden breakpoint is zero)
 * 恢复TLB中的HOOK页信息
 * @warning This function won't remove entries from LIST_ENTRY,
 *  just invalidate the paging, use EptHookUnHookSingleAddress instead
 * 
 * 
 * @param PhysicalAddress 
 * @return BOOLEAN Return false if there was an error or returns true if it was successfull
 */
BOOLEAN
EptHookRestoreSingleHookToOrginalEntry(SIZE_T PhysicalAddress)
{
    PLIST_ENTRY TempList = 0;

    //
    // Should be called from vmx-root, for calling from vmx non-root use the corresponding VMCALL
    //
    if (!g_GuestState[KeGetCurrentProcessorNumber()].IsOnVmxRootMode)
    {
        return FALSE;
    }

    TempList = &g_EptState->HookedPagesList;
    while (&g_EptState->HookedPagesList != TempList->Flink)
    {
        TempList                            = TempList->Flink;
        PEPT_HOOKED_PAGE_DETAIL HookedEntry = CONTAINING_RECORD(TempList, EPT_HOOKED_PAGE_DETAIL, PageHookList);
        if (HookedEntry->PhysicalBaseAddress == PAGE_ALIGN(PhysicalAddress))
        {
            //
            // Undo the hook on the EPT table
            // 使TLS缓存失效
            //
            EptSetPML1AndInvalidateTLB(HookedEntry->EntryAddress, HookedEntry->OriginalEntry, INVEPT_SINGLE_CONTEXT);

            return TRUE;
        }
    }
    //
    // Nothing found, probably the list is not found
    //
    return FALSE;
}

/**
 * @brief Remove and Invalidate Hook in TLB
 * 移除所有TLB中的HOOK信息
 * @warning 此功能不会从列表条目中删除条目，只会使分页无效，请改用EptHookUnHookAll
 * 
 * @return VOID 
 */
VOID
EptHookRestoreAllHooksToOrginalEntry()
{
    PLIST_ENTRY TempList = 0;

    //
    // 应该从vmx-root调用，对于从vmx-no-root调用，请使用相应的VMCALL
    //
    if (!g_GuestState[KeGetCurrentProcessorNumber()].IsOnVmxRootMode)
    {
        return FALSE;
    }

    TempList = &g_EptState->HookedPagesList;

    while (&g_EptState->HookedPagesList != TempList->Flink)
    {
        TempList                            = TempList->Flink;
        PEPT_HOOKED_PAGE_DETAIL HookedEntry = CONTAINING_RECORD(TempList, EPT_HOOKED_PAGE_DETAIL, PageHookList);

        //
        // Undo the hook on the EPT table
        //
        EptSetPML1AndInvalidateTLB(HookedEntry->EntryAddress, HookedEntry->OriginalEntry, INVEPT_SINGLE_CONTEXT);
    }
}

/**
 * @brief 将绝对x64跳转写入缓冲区的任意地址
 * 
 * @param TargetBuffer 
 * @param TargetAddress 
 * @return VOID 
 */
VOID
EptHookWriteAbsoluteJump(PCHAR TargetBuffer, SIZE_T TargetAddress)
{
    //
    // call $ + 5 ; A 64-bit call instruction is still 5 bytes wide!
    //
    TargetBuffer[0] = 0xe8;
    TargetBuffer[1] = 0x00;
    TargetBuffer[2] = 0x00;
    TargetBuffer[3] = 0x00;
    TargetBuffer[4] = 0x00;
    //
    // mov r11, Target
    //
    TargetBuffer[5] = 0x49;
    TargetBuffer[6] = 0xBB;

    //
    // Target
    //
    *((PSIZE_T)&TargetBuffer[7]) = TargetAddress;

    //
    // push r11
    //
    TargetBuffer[15] = 0x41;
    TargetBuffer[16] = 0x53;

    //
    // ret
    //
    TargetBuffer[17] = 0xC3;
}

/**
 * @brief 将绝对x64跳转写入缓冲区的任意地址
 * 
 * @param TargetBuffer 
 * @param TargetAddress 
 * @return VOID 
 */
VOID
EptHookWriteAbsoluteJump2(PCHAR TargetBuffer, SIZE_T TargetAddress)
{
    //
    // mov r11, Target
    //
    // push Lower 4-byte TargetAddress
    //
    TargetBuffer[0] = 0x68;

    *((PUINT32)&TargetBuffer[1]) = (UINT32)TargetAddress;

    TargetBuffer[5] = 0xC7;
    TargetBuffer[6] = 0x44;
    TargetBuffer[7] = 0x24;
    TargetBuffer[8] = 0x04;

    *((PUINT32)&TargetBuffer[9]) = (UINT32)(TargetAddress >> 32);

    TargetBuffer[13] = 0xC3;

}

/**
 * @brief Hook ins
 * 写入HOOK
 * 
 * @param Hook 挂钩页面的详细信息
 * @param ProcessCr3 目标进程CR3
 * @param TargetFunction 需要挂钩的目标函数
 * @param TargetFunctionInSafeMemory 跳板代码指针（用于汇编指令计算）Target content in the safe memory (used in Length Disassembler Engine)
 * @param HookFunction 钩子触发时将调用的函数
 * @return BOOLEAN 如果挂钩成功，则返回true；否则，返回false。
 */
BOOLEAN
EptHookInstructionMemory(PEPT_HOOKED_PAGE_DETAIL Hook, CR3_TYPE ProcessCr3, PVOID TargetFunction, PVOID TargetFunctionInSafeMemory, PVOID HookFunction)
{
    PHIDDEN_HOOKS_DETOUR_DETAILS DetourHookDetails;
    SIZE_T                       SizeOfHookedInstructions;
    SIZE_T                       OffsetIntoPage;
    CR3_TYPE                     Cr3OfCurrentProcess;

    OffsetIntoPage = ADDRMASK_EPT_PML1_OFFSET((SIZE_T)TargetFunction);
    LogInfo("The OffsetIntoPage: 0x%llx\n", OffsetIntoPage);

    if ((OffsetIntoPage + 14) > PAGE_SIZE - 1)
    {
        LogError("Function extends past a page boundary. We just don't have the technology to solve this.....\n");
        return FALSE;
    }

    //
    // 确定使用Length Disassembler Engine覆盖所需的指令长度
    //
    //for (SizeOfHookedInstructions = 0;
    //     SizeOfHookedInstructions < 13; //原始hook跳板长度为18，这里改为13
    //     SizeOfHookedInstructions += ldisasm(((UINT64)TargetFunctionInSafeMemory + SizeOfHookedInstructions), TRUE))
    //{
        //
        // Get the full size of instructions necessary to copy
        //
   //}
	//LogInfo("Number of bytes of instruction mem: %d\n", SizeOfHookedInstructions);

	for (SizeOfHookedInstructions = 0;
		SizeOfHookedInstructions < 14;
		SizeOfHookedInstructions += LDE((UINT64)TargetFunctionInSafeMemory + SizeOfHookedInstructions, 64))
	{
		// Get the full size of instructions necessary to copy
	}

    //【Fix】因为for是从0开始的，所以统计出来的长度差了1
    //SizeOfHookedInstructions += 1;

    LogInfo("Number of bytes of instruction mem: %d\n", SizeOfHookedInstructions);

    //
    // Build a trampoline
    //

    //
    // 为跳板分配一些可执行的内存
    //
    Hook->Trampoline = PoolManagerRequestPool(EXEC_TRAMPOLINE, TRUE, MAX_EXEC_TRAMPOLINE_SIZE);

    if (!Hook->Trampoline)
    {
        LogError("Could not allocate trampoline function buffer.\n");
        return FALSE;
    }

    //
    // Copy the trampoline instructions in
    //

    // 切换到目标进程
    //
    Cr3OfCurrentProcess = SwitchOnAnotherProcessMemoryLayoutByCr3(ProcessCr3);

    //
    // The following line can't be used in user mode addresses
    // 这里将原始代码保存到跳板内存中
    // RtlCopyMemory(Hook->Trampoline, TargetFunction, SizeOfHookedInstructions);
    //
    MemoryMapperReadMemorySafe(TargetFunction, Hook->Trampoline, SizeOfHookedInstructions);

    //
    // 恢复到原始进程
    //
    RestoreToPreviousProcess(Cr3OfCurrentProcess);

    //
    // Add the absolute jump back to the original function
	// 在跳板内存尾部构造一个跳转，用于跳回原始代码
    //
    EptHookWriteAbsoluteJump2(&Hook->Trampoline[SizeOfHookedInstructions], (SIZE_T)TargetFunction + SizeOfHookedInstructions);
   // EptHookWriteAbsoluteJump2(&Hook->Trampoline[SizeOfHookedInstructions], (SIZE_T)TargetFunction + SizeOfHookedInstructions);

    LogInfo("Trampoline Address: 0x%llx\n", Hook->Trampoline);
    LogInfo("Hook Function: 0x%llx\n", HookFunction);

    //
    // Let the hook function call the original function
    //
    // *OrigFunction = Hook->Trampoline;
    //

    //
    // Create the structure to return for the debugger, we do it here because it's the first
    // function that changes the original function and if our structure is no ready after this
    // fucntion then we probably see BSOD on other cores
    // 
    // 从池里申请一块内存
    DetourHookDetails                        = PoolManagerRequestPool(DETOUR_HOOK_DETAILS, TRUE, sizeof(HIDDEN_HOOKS_DETOUR_DETAILS));
    DetourHookDetails->HookedFunctionAddress = TargetFunction;
    DetourHookDetails->ReturnAddress         = Hook->Trampoline;

    //
    // 保存Detour Hook Details的地址，因为我们想在挂钩完成后重新分配它
    //
    Hook->AddressOfEptHook2sDetourListEntry = DetourHookDetails;

    //
    // 将其插入到挂钩页面列表中
    //

    InsertHeadList(&g_EptHook2sDetourListHead, &(DetourHookDetails->OtherHooksList));

    //
    // Write the absolute jump to our shadow page memory to jump to our hook
	// 最后在影子页写入跳转代码，实现hook
    EptHookWriteAbsoluteJump2(&Hook->FakePageContents[OffsetIntoPage], (SIZE_T)HookFunction);

    return TRUE;
}

/**
 * @brief The main function that performs EPT page hook with hidden detours and monitor
 * @details 如果VM已初始化，则此函数在VMX非根模式下返回false
 * 必须在VMX根模式下通过VMCALL调用此函数
 * 
 * @param TargetAddress 功能地址或要挂接的内存地址
 * @param HookFunction 钩子触发时将调用的函数
 * @param ProcessCr3 挂钩的进程cr3
 * @param UnsetRead 挂钩读取访问
 * @param UnsetWrite 挂钩写访问
 * @param UnsetExecute 挂钩执行权限
 * @return BOOLEAN 如果挂钩成功，则返回true；如果出现错误，则返回false
 */
//root模式建立HOOK页
BOOLEAN
EptHookPerformPageHook2(PVOID TargetAddress, PVOID HookFunction, CR3_TYPE ProcessCr3, BOOLEAN UnsetRead, BOOLEAN UnsetWrite, BOOLEAN UnsetExecute)
{
    EPT_PML1_ENTRY          ChangedEntry;
    SIZE_T                  PhysicalBaseAddress;
    PVOID                   VirtualTarget;
    PVOID                   TargetBuffer;
    UINT64                  TargetAddressInSafeMemory;
    UINT64                  PageOffset;
    PEPT_PML1_ENTRY         TargetPage;
    PEPT_HOOKED_PAGE_DETAIL HookedPage;
    ULONG                   LogicalCoreIndex;
    CR3_TYPE                Cr3OfCurrentProcess;
    PLIST_ENTRY             TempList    = 0;
    PEPT_HOOKED_PAGE_DETAIL HookedEntry = NULL;

    //
    // Check whether we are in VMX Root Mode or Not
	// 获取当前CPU核心
    //
    LogicalCoreIndex = KeGetCurrentProcessorIndex();

	//当前核心不是根模式就退出
    if (g_GuestState[LogicalCoreIndex].IsOnVmxRootMode && !g_GuestState[LogicalCoreIndex].HasLaunched)
    {
        return FALSE;
    }

    //
    // 将页面从物理地址转换为虚拟地址，以便我们读取其内存。
    // 如果物理地址尚未映射到虚拟内存中，则此函数将返回NULL。
	// 计算虚拟地址的页地址
    //


    VirtualTarget = PAGE_ALIGN(TargetAddress);

    // Find cr3 of target core
    // 根据CR3计算物理地址
    PhysicalBaseAddress = (SIZE_T)VirtualAddressToPhysicalAddressByProcessCr3(VirtualTarget, ProcessCr3);

    if (!PhysicalBaseAddress)
    {
        LogError("Target address could not be mapped to physical memory 0x%llx\n", VirtualTarget);
        return FALSE;
    }

    //
    // try to see if we can find the address
    //
	// 查找是否已经HOOK过了，如果hook过则放弃
    TempList = &g_EptState->HookedPagesList;

    while (&g_EptState->HookedPagesList != TempList->Flink)
    {
        TempList    = TempList->Flink;
        HookedEntry = CONTAINING_RECORD(TempList, EPT_HOOKED_PAGE_DETAIL, PageHookList);

        if (HookedEntry->PhysicalBaseAddress == PhysicalBaseAddress)
        {
            LogInfo("The memory page has been HOOK :（\n");
            //
            // 意味着我们找到了地址，并且epthook2不支持页面上的多个断点
            //
            return FALSE;
        }
    }

    //
    // Set target buffer, request buffer from pool manager,
    // we also need to allocate new page to replace the current page ASAP
    //
	//从池里分配一个新页
    TargetBuffer = PoolManagerRequestPool(SPLIT_2MB_PAGING_TO_4KB_PAGE, TRUE, sizeof(VMM_EPT_DYNAMIC_SPLIT));

	//分配失败则放弃
    if (!TargetBuffer)
    {
        LogError("There is no pre-allocated buffer available\n");
        return FALSE;
    }

	//将2M页面拆分为4K
    if (!EptSplitLargePage(g_EptState->EptPageTable, TargetBuffer, PhysicalBaseAddress, LogicalCoreIndex))
    {
        LogError("Could not split page for the address : 0x%llx\n", PhysicalBaseAddress);
        return FALSE;
    }

    //
    // 查找物理地址的Pml1结构
    //
    TargetPage = EptGetPml1Entry(g_EptState->EptPageTable, PhysicalBaseAddress);

    //
    // 确保目标有效
    //
    if (!TargetPage)
    {
        LogError("Failed to get PML1 entry of the target address\n");
        return FALSE;
    }

    //
    // 从原始页复制PML1信息到假页里
    //
    ChangedEntry = *TargetPage;

    //
    // Execution is treated differently
	// 设置假页面权限，
    //
  
    //正常hook的话是 False    Fasle    True
    //假页面的权限是 禁止读  禁止写  允许执行
    if (UnsetRead)
        ChangedEntry.ReadAccess = 1;    //允许读
    else
        ChangedEntry.ReadAccess = 0;    //禁止读
    //同上
    if (UnsetWrite)
        ChangedEntry.WriteAccess = 1;
    else
        ChangedEntry.WriteAccess = 0;
    //同上
    if (UnsetExecute)
        ChangedEntry.ExecuteAccess = 1;
    else
        ChangedEntry.ExecuteAccess = 0;
   
    //
    // Save the detail of hooked page to keep track of it
    //
	//从池中分配一个HOOK页
    HookedPage = PoolManagerRequestPool(TRACKING_HOOKED_PAGES, TRUE, sizeof(EPT_HOOKED_PAGE_DETAIL));

    if (!HookedPage)
    {
        LogError("There is no pre-allocated pool for saving hooked page details\n");
        return FALSE;
    }

	//设置此HOOK页接管范围
	HookedPage->ReadAccess = !UnsetRead;

	HookedPage->WriteAccess = !UnsetWrite;

	HookedPage->ExecutionAccess = !UnsetExecute;

	// Save the Cr3 Flags
	HookedPage->ProcessCr3.Flags = ProcessCr3.Flags;

    //
    // Save the virtual address
    //
    HookedPage->VirtualAddress = TargetAddress;

    //
    // Save the physical address
    //
    HookedPage->PhysicalBaseAddress = PhysicalBaseAddress;

    //
    // 假页面的物理地址
    //
    HookedPage->PhysicalBaseAddressOfFakePageContents = (SIZE_T)VirtualAddressToPhysicalAddress(&HookedPage->FakePageContents[0]) / PAGE_SIZE;

    //
    // Save the entry address
    //
    HookedPage->EntryAddress = TargetPage;

    //
    // 保存原始
    //
    HookedPage->OriginalEntry = *TargetPage;


    //
    // 如果是执行挂钩，那么我们必须设置额外的字段
    //
    if (UnsetExecute)
    {
        //
        // 显示条目具有隐藏的执行钩子
        //
        HookedPage->IsExecutionHook = TRUE;

        //
        // 在执行挂钩中，我们必须确保取消设置读写，因为在这种情况下会发生EPT冲突，因此我们可以交换原始页面
        // 我们在上面已经根据情况进行设置了
        //ChangedEntry.ReadAccess    = 0; //禁止读
        //ChangedEntry.WriteAccess   = 0; //禁止写
        //ChangedEntry.ExecuteAccess = 1; //允许执行

        //
        // 同时将当前pfn设置为假页面
        //
        ChangedEntry.PageFrameNumber = HookedPage->PhysicalBaseAddressOfFakePageContents;

        //
        // 切换到目标进程
        //
        Cr3OfCurrentProcess = SwitchOnAnotherProcessMemoryLayoutByCr3(ProcessCr3);

        //
		//将内容复制到假页面
        //以下行不能在用户模式地址中使用
        // RtlCopyBytes(&HookedPage->FakePageContents, VirtualTarget, PAGE_SIZE);
        //
        MemoryMapperReadMemorySafe(VirtualTarget, &HookedPage->FakePageContents, PAGE_SIZE);

        //
        // 恢复到原始进程
        //
        RestoreToPreviousProcess(Cr3OfCurrentProcess);

        //
        // 将目标偏移量的新偏移量计算到安全缓冲区中
        // It will be used to compute the length of the detours
        // address because we might have a user mode code
        //
        TargetAddressInSafeMemory = &HookedPage->FakePageContents;
        TargetAddressInSafeMemory = PAGE_ALIGN(TargetAddressInSafeMemory);
        PageOffset                = PAGE_OFFSET(TargetAddress);
        TargetAddressInSafeMemory = TargetAddressInSafeMemory + PageOffset;

        //
        // Create Hook
        //
        if (!EptHookInstructionMemory(HookedPage, ProcessCr3, TargetAddress, TargetAddressInSafeMemory, HookFunction))
        {
            LogError("Could not build the hook.\n");
            return FALSE;
        }
    }

    //
    // 保存假页面
    //
    HookedPage->ChangedEntry = ChangedEntry;

    //
    // Add it to the list
	// 保存hook表
    //
    InsertHeadList(&g_EptState->HookedPagesList, &(HookedPage->PageHookList));

    //
    // 如果未启动，则无需在安全的环境中对其进行修改
    //
	
    if (!g_GuestState[LogicalCoreIndex].HasLaunched)
    {
        //
        // 将挂钩应用到EPT
        //
        TargetPage->Flags = ChangedEntry.Flags;
    }
    else
    {
        //
        // 将挂钩应用到EPT，让设置生效
        //
      //  EptSetPML1AndInvalidateTLB(TargetPage, ChangedEntry, INVEPT_SINGLE_CONTEXT);

		
    }
    return TRUE;
}

/**
 * @brief 此函数在VMX非根模式下分配缓冲区，然后调用VMCALL设置挂钩
 * @details this command uses hidden detours, this NOT be called from vmx-root mode
 *
 *
 * @param TargetAddress 功能地址或要挂接的内存地址
 * @param HookFunction 钩子触发时将调用的函数
 * @param ProcessId 根据该进程的cr3进行翻译的进程ID
 * @param SetHookForRead 挂钩读取访问
 * @param SetHookForWrite 挂钩写访问
 * @param SetHookForExec 挂钩执行权限
 * @return BOOLEAN 如果挂钩成功，则返回true；如果出现错误，则返回false
 * 
 */
PVOID
EptHook2(PVOID TargetAddress, PVOID HookFunction, UINT32 ProcessId, BOOLEAN eSetHookForRead, BOOLEAN eSetHookForWrite, BOOLEAN eSetHookForExec)
{
    UINT32 PageHookMask = 0;
    ULONG  LogicalCoreIndex;

    //
    // Check for the features to avoid EPT Violation problems
	//检查避免EPT异常
    //

    if (eSetHookForExec && !g_ExecuteOnlySupport)
    {
        //
        // 在当前的hyperdbg设计中，我们使用仅执行页面为exec页面实现隐藏的钩子，因此您的处理器没有此功能，您必须以其他方式实现它：(
        //
        return NULL;
    }
	
    if (eSetHookForWrite && !eSetHookForRead)
    {
        //
        // 隐藏启用写启用和禁用读的钩子将导致违反EPT！
        //
        return NULL;
    }

    //
    // Check whether we are in VMX Root Mode or Not
	// 获取下当前CPU的ID
    //
    LogicalCoreIndex = KeGetCurrentProcessorIndex();

    if (eSetHookForRead)
    {
        PageHookMask |= PAGE_ATTRIB_READ;
    }
    if (eSetHookForWrite)
    {
        PageHookMask |= PAGE_ATTRIB_WRITE;
    }
    if (eSetHookForExec)
    {
        PageHookMask |= PAGE_ATTRIB_EXEC;
    }

    if (PageHookMask == 0)
    {
        //
        // nothing to hook
        //
        return NULL;
    }

	//当前CPU是否虚拟化状态
    if (g_GuestState[LogicalCoreIndex].HasLaunched)
	//虚拟化处理
    {
        //
        // Move Attribute Mask to the upper 32 bits of the VMCALL Number
		//设置标志位，使用vmcall在物理机下进行HOOK
        //
        UINT64 VmcallNumber = ((UINT64)PageHookMask << 32) | VMCALL_CHANGE_PAGE_ATTRIB;

        //使用VmCall在根模式设置钩子和EPT
        if (AsmVmxVmcall(VmcallNumber, TargetAddress, HookFunction, GetCr3FromProcessId(ProcessId).Flags) == STATUS_SUCCESS)
        {
            if (!g_GuestState[LogicalCoreIndex].IsOnVmxRootMode)
            {
                //
                // Now we have to notify all the core to invalidate their EPT
                // 通知所有EPT无效，让CPU刷新
                //
				// 不要在这里刷新
				//HvNotifyAllToInvalidateEpt();
            }
            else
            {
                LogError("Unable to notify all cores to invalidate their TLB caches as you called hook on vmx-root mode.\n");
            }
			//hook完毕后返回指针
            return EptHookResultTrampoline(TargetAddress);
        }
    }
    else
	//非虚拟化状态
    {
		 LogInfo("[*]VM has not launched call EptHookPerformPageHook2\n");
    }
    LogWarning("Hook not applied\n");

    return NULL;
}


/**
 * @brief Knernel Api Hook
 *
 * @param HookFunction 钩子触发时将调用的函数
 * @param ApiName 要HOOK的内核导出函数
 * @return 成功返回跳板地址，失败则返回Null
 */
PVOID
EptHookApiInPid(PVOID HookFunction, PCWSTR ApiName, UINT32 Pid)
{
    UNICODE_STRING StringNtCreateFile;
    RtlInitUnicodeString(&StringNtCreateFile, ApiName);
	PVOID ApiLocation = MmGetSystemRoutineAddress(&StringNtCreateFile);

    if (ApiLocation == NULL)
    {
    ApiLocation = (PVOID)FindPdbAddress(StringNtCreateFile);
    }
    return EptHook2(ApiLocation, HookFunction, (UINT32)PsGetCurrentProcessId(), (BOOLEAN)FALSE, (BOOLEAN)FALSE, (BOOLEAN)TRUE);
}
/**
 * @brief Knernel Api Hook
 *
 * @param HookFunction 钩子触发时将调用的函数
 * @param ApiName 要HOOK的内核导出函数
 * @return 成功返回跳板地址，失败则返回Null
 */
PVOID 
EptHookApi(PVOID HookFunction, PCWSTR ApiName)
{
    return EptHookApiInPid(HookFunction,ApiName,(UINT32)PsGetCurrentProcessId());
}

PVOID
EptHookApiByPoint(PVOID HookFunction, PVOID ApiFun)
{
	return EptHook2(ApiFun, HookFunction, (UINT32)PsGetCurrentProcessId(), (BOOLEAN)FALSE, (BOOLEAN)FALSE, (BOOLEAN)TRUE);
}

    /**
 * @brief Handles page hooks
 * 
 * @param Regs Guest registers
 * @param HookedEntryDetails The entry that describes the hooked page
 * @param ViolationQualification The exit qualification of vm-exit
 * @param PhysicalAddress The physical address that cause this vm-exit
 * @return BOOLEAN Returns TRUE if the function was hook was handled or returns false 
 * if there was an unexpected ept violation
 */
BOOLEAN
EptHookHandleHookedPage(PGUEST_REGS Regs, EPT_HOOKED_PAGE_DETAIL * HookedEntryDetails, VMX_EXIT_QUALIFICATION_EPT_VIOLATION ViolationQualification, SIZE_T PhysicalAddress)
{
	ULONG64 GuestRip;
	ULONG64 ExactAccessedAddress;
	ULONG64 AlignedVirtualAddress;
	ULONG64 AlignedPhysicalAddress;
	BOOLEAN IsReadWrite;

	UNREFERENCED_PARAMETER(Regs);
	IsReadWrite = TRUE;
	//
	// 获取HOOK页虚拟地址的对齐地址
	//
	AlignedVirtualAddress = PAGE_ALIGN(HookedEntryDetails->VirtualAddress);
    //计算引发Exit物理地址所在的页地址
	AlignedPhysicalAddress = PAGE_ALIGN(PhysicalAddress);
    
	//
	// 让我们阅读访问的确切地址
    // 计算HOOK页上的偏移地址（虚拟地址）
	//
	ExactAccessedAddress = AlignedVirtualAddress + PhysicalAddress - AlignedPhysicalAddress;

	//
	// 阅读Guest的RIP
	//
	__vmx_vmread(GUEST_RIP, &GuestRip);

	IsReadWrite = FALSE;
	if (!ViolationQualification.EptExecutable && ViolationQualification.ExecuteAccess)
	{
		//执行异常
		if (!HookedEntryDetails->ExecutionAccess)
		{
			return FALSE;
		}
	}
	else
	if (!ViolationQualification.EptReadable && ViolationQualification.ReadAccess)
	{
		if (!HookedEntryDetails->ReadAccess)
		{
			return FALSE;
		}
		//读异常
		IsReadWrite = TRUE;
	}
	else
	if (!ViolationQualification.EptWriteable && ViolationQualification.WriteAccess)
	{
		if (!HookedEntryDetails->WriteAccess)
		{
			return FALSE;
		}
		//写异常
		IsReadWrite = TRUE;
	}
	else
	{
		LogError("Unexpected permission status Guest RIP : 0x%llx\n");
	}



	if (IsReadWrite)
	{
		//LogInfo("Guest RIP : 0x%llx tries to read on the page at :0x%llx\n", GuestRip, ExactAccessedAddress);
		//
		// Next we have to save the current hooked entry to restore on the next instruction's vm-exit
		//
		// 记录换页地址，然后设置单步执行
		g_GuestState[KeGetCurrentProcessorNumber()].MtfEptHookRestorePoint = HookedEntryDetails;

		//
		// We have to set Monitor trap flag and give it the HookedEntry to work with
		//
		//单步执行,当前指令执行完后，下一条指令会触发MTF
		HvSetMonitorTrapFlag(TRUE);
	}
	else
	{
		//__vmx_vmwrite(GUEST_RIP, ExactAccessedAddress);
		//执行，不需要单步
		EptSetPML1AndInvalidateTLB(HookedEntryDetails->EntryAddress, HookedEntryDetails->ChangedEntry, INVEPT_SINGLE_CONTEXT);

	}

	EptSetPML1AndInvalidateTLB(HookedEntryDetails->EntryAddress, HookedEntryDetails->OriginalEntry, INVEPT_SINGLE_CONTEXT);


	//
	// 意味着在来宾中执行当前指令后，将Entry恢复到以前的状态
	//
	return TRUE;
}


/**
 * @brief Remove the enrty from g_EptHook2sDetourListHead in the case
 * of !epthook2 details
 * @param Address Address to remove
 * @return BOOLEAN TRUE if successfully removed and false if not found 
 */
BOOLEAN
EptHookRemoveEntryAndFreePoolFromEptHook2sDetourList(UINT64 Address)
{
    PLIST_ENTRY TempList = 0;

    //
    // Iterate through the list of hooked pages details to find
    // the entry in the list
    //
    TempList = &g_EptHook2sDetourListHead;

    while (&g_EptHook2sDetourListHead != TempList->Flink)
    {
        TempList                                          = TempList->Flink;
        PHIDDEN_HOOKS_DETOUR_DETAILS CurrentHookedDetails = CONTAINING_RECORD(TempList, HIDDEN_HOOKS_DETOUR_DETAILS, OtherHooksList);

        if (CurrentHookedDetails->HookedFunctionAddress == Address)
        {
            //
            // We found the address, we should remove it and add it for
            // future deallocation
            //
            RemoveEntryList(&CurrentHookedDetails->OtherHooksList);

            //
            // Free the pool in next ioctl
            //
            if (!PoolManagerFreePool(CurrentHookedDetails))
            {
                LogError("Something goes wrong ! the pool not found in the list of previously allocated pools by pool manager.\n");
            }
            return TRUE;
        }
    }
    //
    // No entry found !
    //
    return FALSE;
}

/**
 * @brief Remove single hook from the hooked pages list and invalidate TLB
 * @details Should be called from vmx non-root
 * 
 * @param VirtualAddress Virtual address to unhook
 * @param ProcessId The process id of target process
 * @return BOOLEAN If unhook was successful it returns true or if it was not successful returns false
 */
BOOLEAN
EptHookUnHookSingleAddress(UINT64 VirtualAddress, UINT32 ProcessId)
{
    SIZE_T      PhysicalAddress;
//    UINT64      TargetAddressInFakePageContent;
//    UINT64      PageOffset;
    PLIST_ENTRY TempList                   = 0;
//    BOOLEAN     FoundHiddenBreakpointEntry = FALSE;

	//DbgBreakPoint();
	//判断下进程ID是0或者fff，如果是则获取当前进程
    if (ProcessId == DEBUGGER_EVENT_APPLY_TO_ALL_PROCESSES || ProcessId == 0)
    {
        ProcessId = PsGetCurrentProcessId();
    }

	//计算物理地址
    PhysicalAddress = PAGE_ALIGN(VirtualAddressToPhysicalAddressByProcessId(VirtualAddress, ProcessId));

    //
    // Should be called from vmx non-root
    //
    if (g_GuestState[KeGetCurrentProcessorNumber()].IsOnVmxRootMode)
    {
        return FALSE;
    }

	
	//搜索链表
    TempList = &g_EptState->HookedPagesList;

    while (&g_EptState->HookedPagesList != TempList->Flink)
    {
        TempList                            = TempList->Flink;
        PEPT_HOOKED_PAGE_DETAIL HookedEntry = CONTAINING_RECORD(TempList, EPT_HOOKED_PAGE_DETAIL, PageHookList);	
        //
        // It's a hidden detours
        //
		// 简单的很
        if (HookedEntry->PhysicalBaseAddress == PhysicalAddress)
        {
            //
            // Remove it in all the cores
            //
            KeGenericCallDpc(HvDpcBroadcastRemoveHookAndInvalidateSingleEntry, HookedEntry->PhysicalBaseAddress);

            //
            // Now that we removed this hidden detours hook, it is
            // time to remove it from g_EptHook2sDetourListHead
            //
            EptHookRemoveEntryAndFreePoolFromEptHook2sDetourList(HookedEntry->VirtualAddress);

            //
            // remove the entry from the list
            //
			if (!RemoveEntryList(HookedEntry->PageHookList.Flink))
			{
				LogError("RemoveEntryList Error.\n");
			}
            //
            // we add the hooked entry to the list
            // of pools that will be deallocated on next IOCTL
            //
            if (!PoolManagerFreePool(HookedEntry))
            {
                LogError("Something goes wrong ! the pool not found in the list of previously allocated pools by pool manager.\n");
            }

            return TRUE;
        }
    }
    //
    // Nothing found , probably the list is not found
    //
    return FALSE;
}

/**
 * @brief 从挂钩页面列表中删除所有挂钩，并使TLB无效
 * @detailsShould be called from Vmx Non-root
 * 
 * @return VOID 
 */
// 循环移除HOOK

VOID
EptRemoveHookAndInvalidateAllEntries()
{
	if (g_GuestState[KeGetCurrentProcessorNumber()].IsOnVmxRootMode)
	{
		return;
	}
	//不需要刷新全部缓存
	//KeGenericCallDpc(HvDpcBroadcastRemoveHookAndInvalidateAllEntries, 0x0);
	PLIST_ENTRY TempList = 0;
	TempList = &g_EptState->HookedPagesList;

	while (&g_EptState->HookedPagesList != TempList->Flink)
	{
		TempList = TempList->Flink;
		PEPT_HOOKED_PAGE_DETAIL HookedEntry = CONTAINING_RECORD(TempList, EPT_HOOKED_PAGE_DETAIL, PageHookList);

		//循环移除所有钩子的TSL
		KeGenericCallDpc(HvDpcBroadcastRemoveHookAndInvalidateSingleEntry, HookedEntry->PhysicalBaseAddress);

		//	HvNotifyAllToInvalidateEpt();
	}
}

VOID
EptHookUnHookAll()
{
    PLIST_ENTRY TempList = 0;

    //
    // Should be called from vmx non-root
    //


    if (g_GuestState[KeGetCurrentProcessorNumber()].IsOnVmxRootMode)
    {
        return;
    }

    TempList = &g_EptState->HookedPagesList;

    while (&g_EptState->HookedPagesList != TempList->Flink)
    {
        TempList                            = TempList->Flink;
        PEPT_HOOKED_PAGE_DETAIL HookedEntry = CONTAINING_RECORD(TempList, EPT_HOOKED_PAGE_DETAIL, PageHookList);

        //
        // Now that we removed this hidden detours hook, it is time to remove it from g_EptHook2sDetourListHead if the hook is detours
        //
		//KeGenericCallDpc(HvDpcBroadcastRemoveHookAndInvalidateSingleEntry, HookedEntry->PhysicalBaseAddress);

		LogWarning("Remove Hook 0x%llx Success\n", HookedEntry->VirtualAddress);
		// 从链表中移除hook页
		if (!RemoveEntryList(TempList))
		{

		}

        if (!PoolManagerFreePool(HookedEntry))
        {
            LogError("Something goes wrong ! the pool not found in the list of previously allocated pools by pool manager.\n");
        }
	//	HvNotifyAllToInvalidateEpt();
    }
}

/**
 * @brief 修改TLB并使缓存失效
 * @detailsShould be called from Vmx Non-root
 *
 * @return VOID
 */
VOID EptHookFlushAndInvalidateTLB()
{
	AsmVmxVmcall(VMCALL_FLUSH_EPTHOOK, 0, 0, 0);
}

/**
 * @brief 修改TLB并使缓存失效，请勿在非根模式使用此函数
 * @detailsShould be called from Vmx Non-root
 *
 * @return VOID
 */
VOID HvEptHookFlushAndInvalidateTLB()
{

	PLIST_ENTRY TempList = &g_EptState->HookedPagesList;
	while (&g_EptState->HookedPagesList != TempList->Flink)
	{
		TempList = TempList->Flink;
		PEPT_HOOKED_PAGE_DETAIL HookedEntry = CONTAINING_RECORD(TempList, EPT_HOOKED_PAGE_DETAIL, PageHookList);

		//该节点是epthook节点
		if (HookedEntry->IsExecutionHook)
		{
			    LogWarning("Start Hook 0x%llx Success\n", HookedEntry->VirtualAddress);
				EptSetPML1AndInvalidateTLB(HookedEntry->EntryAddress, HookedEntry->ChangedEntry, INVEPT_SINGLE_CONTEXT);
				//return (HookedEntry->Trampoline);
		}
	}
}

/**
 * @brief 从Hook页表中搜索跳板函数地址
 * @detailsShould be called from Vmx Non-root
 *
 * @return VOID
 */
PVOID EptHookResultTrampoline(UINT64 VirtualAddress)
{
	PLIST_ENTRY TempList = &g_EptState->HookedPagesList;
	while (&g_EptState->HookedPagesList != TempList->Flink)
	{
		TempList = TempList->Flink;
		PEPT_HOOKED_PAGE_DETAIL HookedEntry = CONTAINING_RECORD(TempList, EPT_HOOKED_PAGE_DETAIL, PageHookList);

		//该节点是epthook节点
		if (HookedEntry->IsExecutionHook)
		{
			if (HookedEntry->VirtualAddress == VirtualAddress)
			{
				return (HookedEntry->Trampoline);
			}
		}
	}
	return NULL;

	
}
