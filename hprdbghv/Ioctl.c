/**
 * @file Ioctl.c
 * @author Sina Karvandi (sina@rayanfam.com)
 * @brief IOCTL Functions form user mode and other parts 
 * @details 
 *
 * @version 0.1
 * @date 2020-06-01
 * 
 * @copyright This project is released under the GNU Public License v3.
 * 
 */
#include "pch.h"


NTSTATUS
RegisterEventBased(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
    PNOTIFY_RECORD          NotifyRecord;
    NTSTATUS                Status;
    PIO_STACK_LOCATION      IrpStack;
    PREGISTER_NOTIFY_BUFFER RegisterEvent;
    KIRQL                   OldIrql;

    IrpStack      = IoGetCurrentIrpStackLocation(Irp);
    RegisterEvent = (PREGISTER_NOTIFY_BUFFER)Irp->AssociatedIrp.SystemBuffer;

    //
    // Allocate a record and save all the event context
    //
    NotifyRecord = ExAllocatePoolWithQuotaTag(NonPagedPool, sizeof(NOTIFY_RECORD), POOLTAG);

    if (NULL == NotifyRecord)
    {
        return STATUS_INSUFFICIENT_RESOURCES;
    }

    NotifyRecord->Type = EVENT_BASED;

    //
    // Get the object pointer from the handle
    // Note we must be in the context of the process that created the handle
    //
    Status = ObReferenceObjectByHandle(RegisterEvent->hEvent,
                                       SYNCHRONIZE | EVENT_MODIFY_STATE,
                                       *ExEventObjectType,
                                       Irp->RequestorMode,
                                       &NotifyRecord->Message.Event,
                                       NULL);

    if (!NT_SUCCESS(Status))
    {
        LogError("Unable to reference User-Mode Event object, Error = 0x%x", Status);
        ExFreePoolWithTag(NotifyRecord, POOLTAG);
        return Status;
    }

	if (g_R3mdl != NULL)
	{
		IoFreeMdl(g_R3mdl);
	}

	g_R3mdl = IoAllocateMdl(RegisterEvent->Buffer, 4096, FALSE, FALSE, NULL);

	if (g_R3mdl == NULL)
	{
		LogError("mdl error");
	}

	
	MmBuildMdlForNonPagedPool(g_R3mdl);

	g_R3Memory = MmGetSystemAddressForMdlSafe(g_R3mdl, HighPagePriority);
	
    //保存，需要构造一个灵活的R3事件存储代码
    g_R3Event = RegisterEvent->hEvent;
    ObDereferenceObject(NotifyRecord->Message.Event);
    ExFreePoolWithTag(NotifyRecord, POOLTAG);
    return STATUS_SUCCESS;
}


/**
 * @brief 用户层事件句柄转内核句柄并触发
 * 
 * @UserModeEvent 用户层句柄
 * @return NTSTATUS 
 */
NTSTATUS
QueryEventAndSetEvent(HANDLE UserModeEvent)
{
    PNOTIFY_RECORD NotifyRecord;
    NTSTATUS       Status;
   // KeSetEvent(G, 0, FALSE);
    //
    // Allocate a record and save all the event context
    //
    NotifyRecord = ExAllocatePoolWithQuotaTag(NonPagedPool, sizeof(NOTIFY_RECORD), POOLTAG);

    if (NULL == NotifyRecord)
    {
        return STATUS_INSUFFICIENT_RESOURCES;
    }
    NotifyRecord->Type = EVENT_BASED;

    //
    // Get the object pointer from the handle
    // Note we must be in the context of the process that created the handle
    //
    Status = ObReferenceObjectByHandle(UserModeEvent,
                                       SYNCHRONIZE | EVENT_MODIFY_STATE,
                                       *ExEventObjectType,
                                       UserMode,
                                       &NotifyRecord->Message.Event,
                                       NULL);


    if (Status == STATUS_INVALID_HANDLE)
    {
        LogError("The EVENT does not exist, Error = 0x%x", Status);
    }
    else
    if (!NT_SUCCESS(Status))
    {
        LogError("Unable to reference User-Mode Event object, Error = 0x%x", Status);
        ExFreePoolWithTag(NotifyRecord, POOLTAG);
        return Status;
    }

    KeSetEvent(NotifyRecord->Message.Event, 0, FALSE);
    ObDereferenceObject(NotifyRecord->Message.Event);
    ExFreePoolWithTag(NotifyRecord, POOLTAG);
    return STATUS_SUCCESS;
}


NTSTATUS
IoStartHideDebug(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
    PIO_STACK_LOCATION      IrpStack;
    PHIDE_AND_TRANSPARENT_DEBUGGER_MODE Measurements;
    USHORT                              Size;

    IrpStack      = IoGetCurrentIrpStackLocation(Irp);
    Measurements = (PREGISTER_NOTIFY_BUFFER)Irp->AssociatedIrp.SystemBuffer;

    return TransparentHideDebugger(Measurements);
}

NTSTATUS
IoPdbAddress(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	PIO_STACK_LOCATION      IrpStack;
	PREGISTER_PDFADDRESS Measurements;
    PREGISTER_PDFADDRESS_LIST List;
	USHORT                              Size;

	IrpStack = IoGetCurrentIrpStackLocation(Irp);
	Measurements = (PREGISTER_PDFADDRESS)Irp->AssociatedIrp.SystemBuffer;


    //申请内存
    List = ExAllocatePool(NonPagedPool, sizeof(REGISTER_PDFADDRESSR_LIST));

    //如果申请不到则放弃
    if (List == NULL)
    {
        return STATUS_MEMORY_NOT_ALLOCATED;
    }

    //初始化符号名字符串
    RtlInitUnicodeString(&List->SymbolName, Measurements->SymbolName);
	//初始化模块字符串
	RtlInitUnicodeString(&List->ImageName, Measurements->ImageName);
	
	//内核偏移 + 符号偏移 = 内核实际地址
    List->Address = Measurements->Address + (UINT64)FindImageBase(&List->ImageName);
    //插入链表
    InsertHeadList(&g_PDBList, &(List->List));
	
    //debug
    LogInfo("Kernel Symbol Name: %wZ Address: 0x%p", List->SymbolName, (PVOID)List->Address);

	return STATUS_SUCCESS;
	//return TransparentHideDebugger(Measurements);
}


//IO处理代码
/**
 * @brief Driver IOCTL Dispatcher
 * 
 * @param DeviceObject 
 * @param Irp 
 * @return NTSTATUS 
 */
NTSTATUS
DrvDispatchIoControl(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
    PIO_STACK_LOCATION                           IrpStack;
    NTSTATUS                                     Status;
    BOOLEAN                                      DoNotChangeInformation = FALSE;


	UNREFERENCED_PARAMETER(DeviceObject);
    //
    // Here's the best place to see if there is any allocation pending
    // to be allcated as we're in PASSIVE_LEVEL
    //
    PoolManagerCheckAndPerformAllocationAndDeallocation();

    if (g_AllowIOCTLFromUsermode)
    {
        IrpStack = IoGetCurrentIrpStackLocation(Irp);

	//	LogError("Unknow IOCTL\n");
		Status = STATUS_NOT_IMPLEMENTED;
        switch (IrpStack->Parameters.DeviceIoControl.IoControlCode)
        {

        case IOCTL_REGISTER_EVENT:
          //  RegisterEventBased(DeviceObject, Irp);
            break;
		case IOCTL_STOPHOOK:
			EptRemoveHookAndInvalidateAllEntries();
            Status = STATUS_SUCCESS;
			break;
        case IOCTL_Hide_Debug:
            Status = IoStartHideDebug(DeviceObject, Irp);
            break;
		case IOCTL_Send_pdb:
            Status = IoPdbAddress(DeviceObject, Irp);
			break;
        case IOCTL_Start_VT:
            Status = OpenHyper();
            break;
        case IOCTL_Stop_VT:
            Status = CloseHyper();
            break;
        default:
            LogError("Unknow IOCTL\n");
            Status = STATUS_NOT_IMPLEMENTED;
            break;
        }
    }
    else
    {
        //
        // We're no longer serve IOCTL
        //
        Status = STATUS_SUCCESS;
    }

    if (Status != STATUS_PENDING)
    {
        Irp->IoStatus.Status = Status;
        if (!DoNotChangeInformation)
        {
            Irp->IoStatus.Information = 0;
        }
        IoCompleteRequest(Irp, IO_NO_INCREMENT);
    }

    return Status;
}
