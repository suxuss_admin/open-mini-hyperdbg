/**
 * @file Driver.c
 * @author Sina Karvandi (sina@rayanfam.com)
 * @brief The project entry 
 * @details This file contains major functions and all the interactions
 * with usermode codes are managed from here.
 * e.g debugger commands and extension commands
 * @version 0.1
 * @date 2020年10月2日
 * 
 * @copyright This project is released under the GNU Public License v3.
 * 
 */
#include "pch.h"
#include "Driver.tmh"




/**
 * @brief 驱动程序加载时的主驱动程序条目
 * 
 * @param DriverObject 
 * @param RegistryPath 
 * @return NTSTATUS 
 */
NTSTATUS
DriverEntry(
    PDRIVER_OBJECT  DriverObject,
    PUNICODE_STRING RegistryPath)
{
    NTSTATUS       Ntstatus       = STATUS_SUCCESS;
    UINT64         Index          = 0;
    UINT32         ProcessorCount = 0;
    
    UNICODE_STRING DriverName     = RTL_CONSTANT_STRING(L"\\Device\\HyperdbgHypervisorDevice");
    UNICODE_STRING DosDeviceName  = RTL_CONSTANT_STRING(L"\\DosDevices\\HyperdbgHypervisorDevice");

    UNREFERENCED_PARAMETER(RegistryPath);
    UNREFERENCED_PARAMETER(DriverObject);

    //
    // Initialize WPP Tracing
    //

    WPP_INIT_TRACING(DriverObject, RegistryPath);
    /*
	if (!LogInitialize())
	{
		DbgPrint("[*] Log buffer is not initialized !\n");
		DbgBreakPoint();
	}*/
    //
    // Opt-in to using non-executable pool memory on Windows 8 and later.
	// 在Win8以上使用不可分页内存池
    // https://msdn.microsoft.com/en-us/library/windows/hardware/hh920402(v=vs.85).aspx
    //

    ExInitializeDriverRuntime(DrvRtPoolNxOptIn);

    //
    // 日志系统引用了g_GuestState来描述vmx-root或vmx-no-root，所以我们在这里初始化g_GuestState
    //

	//获取CPU核心数量
    ProcessorCount = KeQueryActiveProcessorCount(0);

    //
    // 分配全局变量
    //

    g_GuestState = ExAllocatePoolWithTag(NonPagedPool, sizeof(VIRTUAL_MACHINE_STATE) * ProcessorCount, POOLTAG);
    if (!g_GuestState)
    {
        //
        // 分配失败则炸掉，用DbgPrint返回信息
        //

        DbgPrint("Insufficient memory\n");
        DbgBreakPoint();
        return STATUS_INSUFFICIENT_RESOURCES;
    }

    //初始化PDB链表
    InitializePdbList();
    //
    // 清零内存
    //
    RtlZeroMemory(g_GuestState, sizeof(VIRTUAL_MACHINE_STATE) * ProcessorCount);

	g_TransparentMode = FALSE;
	
    LogInfo("Hypervisor is Loaded :)\n");

    Ntstatus = IoCreateDevice(DriverObject,
                              0,
                              &DriverName,
                              FILE_DEVICE_UNKNOWN,
                              FILE_DEVICE_SECURE_OPEN,
                              FALSE,
                              &g_DeviceObjectSave);

    if (Ntstatus == STATUS_SUCCESS)
    {
        for (Index = 0; Index < IRP_MJ_MAXIMUM_FUNCTION; Index++)
            DriverObject->MajorFunction[Index] = DrvUnsupported;

        LogInfo("Setting device major functions\n");
        DriverObject->MajorFunction[IRP_MJ_CLOSE]          = DrvClose;
        DriverObject->MajorFunction[IRP_MJ_CREATE]         = DrvCreate;
        DriverObject->MajorFunction[IRP_MJ_READ]           = DrvRead;
        DriverObject->MajorFunction[IRP_MJ_WRITE]          = DrvWrite;
		DriverObject->MajorFunction[IRP_MJ_SHUTDOWN]	   = DrvShutdown;
        DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = DrvDispatchIoControl;


        DriverObject->DriverUnload = DrvUnload;
        IoCreateSymbolicLink(&DosDeviceName, &DriverName);
    }

    //
    // 建立用户缓冲区访问方法。
    //
    g_DeviceObjectSave->Flags |= DO_BUFFERED_IO;

	//
	// Allow to server IOCTL
	//
	g_AllowIOCTLFromUsermode = TRUE;

	LogInfo("hypervisor Started...\n");

	//
	// Zero the memory
	//
	RtlZeroMemory(g_GuestState, sizeof(VIRTUAL_MACHINE_STATE) * ProcessorCount);

	//
	// Initialize memory mapper
	// 初始化内存映射
	// 一套安全的内存映射器
	//
	MemoryMapperInitialize();

  //  ASSERT(NT_SUCCESS(Ntstatus));
    return Ntstatus;
}

/**
 * @brief 在驱动程序卸载的情况下运行以注销设备
 * 卸载驱动
 * @param DriverObject 
 * @return VOID 
 */
VOID
DrvUnload(PDRIVER_OBJECT DriverObject)
{
    UNICODE_STRING DosDeviceName;

	/*
	if (g_R3mdl != NULL)
    {
        IoFreeMdl(g_R3mdl);
    }*/

	//释放内存映射器
	MemoryMapperUninitialize();

    RtlInitUnicodeString(&DosDeviceName, L"\\DosDevices\\HyperdbgHypervisorDevice");
    IoDeleteSymbolicLink(&DosDeviceName);
    IoDeleteDevice(DriverObject->DeviceObject);

	LogWarning("Uinitializing !bye!\n");
//	LogUnInitialize();
    //
    // 释放g_GuestState，注意，释放以后不能再使用Log系列接口了
    //
	if ARGUMENT_PRESENT(g_GuestState)
	{
		ExFreePoolWithTag(g_GuestState, POOLTAG);
	}
    
    //
    // Stop the tracing
    //
    WPP_CLEANUP(DriverObject);
}


BOOLEAN
FindSubString(
	IN PUNICODE_STRING String,
	IN PUNICODE_STRING SubString
)
{
	ULONG index;

	//
	//  First, check to see if the strings are equal.
	//

	if (RtlEqualUnicodeString(String, SubString, TRUE)) {

		return TRUE;
	}

	//
	//  String and SubString aren't equal, so now see if SubString
	//  in in String any where.
	//
	for (index = 0;
		index + SubString->Length <= String->Length;
		index++) {
		if (_wcsnicmp(&(String->Buffer[index]),
			SubString->Buffer,
			SubString->Length) == 0) {
			//
			//  SubString is found in String, so return TRUE.
		   //
			return TRUE;
		}
	}

	return FALSE;
}


/**
 * @brief IRP_MJ_CREATE Function handler
 * 
 * @param DeviceObject 
 * @param Irp 
 * @return NTSTATUS 
 */
NTSTATUS
DrvCreate(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(DeviceObject);
    int ProcessorCount;

    //
    // Check for privilege
    //
    // Check for the correct security access.
    // The caller must have the SeDebugPrivilege.
    // 验证权限，需要进程有Debug权限，因为去掉了Debug功能，所以没必要验证喽
	/*
    LUID DebugPrivilege = {SE_DEBUG_PRIVILEGE, 0};

    if (!SeSinglePrivilegeCheck(DebugPrivilege, Irp->RequestorMode))
    {
        Irp->IoStatus.Status      = STATUS_ACCESS_DENIED;
        Irp->IoStatus.Information = 0;
        IoCompleteRequest(Irp, IO_NO_INCREMENT);

		//返回权限不足
        return STATUS_ACCESS_DENIED;
    }
	*/

	//
	// 检查以仅允许驱动程序使用一个句柄意味着只有一个应用程序可以获取该句柄，除非调用IRP MJ CLOSE，否则新应用程序将不允许创建新的句柄。
	//

	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = 0;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	//g_VTEnabled = TRUE;
	return STATUS_SUCCESS;

    //
    // if we didn't return by now, means that there is a problem
    //
    //Irp->IoStatus.Status      = STATUS_UNSUCCESSFUL;
    //Irp->IoStatus.Information = 0;
    //IoCompleteRequest(Irp, IO_NO_INCREMENT);

    //return STATUS_UNSUCCESSFUL;
}
NTSTATUS
DrvShutdown(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(DeviceObject);
	LogWarning("DrvShutdown\n");

	CloseHyper();

	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = 0;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return STATUS_SUCCESS;
}


/**
 * @brief IRP_MJ_READ Function handler
 * 响应ReadFile，这里没用
 * @param DeviceObject 
 * @param Irp 
 * @return NTSTATUS 
 */
NTSTATUS
DrvRead(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(DeviceObject);
    LogWarning("Not implemented yet :(\n");

    Irp->IoStatus.Status      = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;
    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return STATUS_SUCCESS;
}

/**
 * @brief IRP_MJ_WRITE Function handler
 * 响应WriteFile，这里没用
 * @param DeviceObject 
 * @param Irp 
 * @return NTSTATUS 
 */
NTSTATUS
DrvWrite(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(DeviceObject);
    LogWarning("Not implemented yet :(\n");

    Irp->IoStatus.Status      = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;
    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return STATUS_SUCCESS;
}

/**
 * @brief IRP_MJ_CLOSE Function handler
 * 响应关闭事件，这里没用
 * @param DeviceObject 
 * @param Irp 
 * @return NTSTATUS 
 */
NTSTATUS
DrvClose(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(DeviceObject);
    //
    // 如果调用了close，则意味着所有IOCTL都不处于挂起状态，因此我们可以安全地允许创建新的句柄，以供将来对驱动程序的调用
    //

    Irp->IoStatus.Status      = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;
    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return STATUS_SUCCESS;
}

/**
 * @brief Unsupported message for all other IRP_MJ_* handlers
 * 未知IRP
 * @param DeviceObject 
 * @param Irp 
 * @return NTSTATUS 
 */
NTSTATUS
DrvUnsupported(PDEVICE_OBJECT DeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(DeviceObject);
    DbgPrint("This function is not supported :(\n");

    Irp->IoStatus.Status      = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;
    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return STATUS_SUCCESS;
}
