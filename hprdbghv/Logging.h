/**
 * @file Logging.h
 * @author Sina Karvandi (sina@rayanfam.com)
 * @brief Headers of Message logging and tracing
 * @details
 * @version 0.1
 * @date 2020-04-11
 * 
 * @copyright This project is released under the GNU Public License v3.
 * 
 */

#pragma once

//////////////////////////////////////////////////
//					Structures					//
//////////////////////////////////////////////////

/**
 * @brief The usermode request
 * 
 */
typedef struct _NOTIFY_RECORD
{
    NOTIFY_TYPE Type;
    union
    {
        PKEVENT Event;
        PIRP    PendingIrp;
    } Message;
    KDPC    Dpc;
    BOOLEAN CheckVmxRootMessagePool; // Set so that notify callback can understand where to check (Vmx root or Vmx non-root)
} NOTIFY_RECORD, *PNOTIFY_RECORD;

/**
 * @brief Message buffer structure
 * 
 */
typedef struct _BUFFER_HEADER
{
    UINT32  OpeationNumber; // Operation ID to user-mode
    UINT32  BufferLength;   // The actual length
    BOOLEAN Valid;          // Determine whether the buffer was valid to send or not
} BUFFER_HEADER, *PBUFFER_HEADER;

/**
 * @brief Core-specific buffers
 * 
 */
typedef struct _LOG_BUFFER_INFORMATION
{
    UINT64 BufferStartAddress; // 缓冲区的起始地址
    UINT64 BufferEndAddress;   // 缓冲区的结束地址

    UINT64 BufferForMultipleNonImmediateMessage; // 缓冲区的起始地址，用于累积非即时消息
    UINT32 CurrentLengthOfNonImmBuffer;          // 用于累积非即时消息的缓冲区的当前大小

    KSPIN_LOCK BufferLock;                 // 自旋锁以保护对队列的访问
    KSPIN_LOCK BufferLockForNonImmMessage; // 自旋锁可保护对非Imm消息队列的访问

    UINT32 CurrentIndexToSend;  // 当前缓冲区索引发送到用户模式
    UINT32 CurrentIndexToWrite; // 当前缓冲区索引以写入新消息

} LOG_BUFFER_INFORMATION, *PLOG_BUFFER_INFORMATION;

//////////////////////////////////////////////////
//				Global Variables				//
//////////////////////////////////////////////////

/**
 * @brief Global Variable for buffer on all cores
 * 
 */
LOG_BUFFER_INFORMATION * MessageBufferInformation;

/**
 * @brief Vmx-root lock for logging
 * 
 */
volatile LONG VmxRootLoggingLock;

/**
 * @brief Vmx-root lock for logging
 * 
 */
volatile LONG VmxRootLoggingLockForNonImmBuffers;

//////////////////////////////////////////////////
//					Illustration				//
//////////////////////////////////////////////////

/*

A core buffer is like this , it's divided into MaximumPacketsCapacity chucks, each chunk has PacketChunkSize + sizeof(BUFFER_HEADER) size

			 _________________________
			|      BUFFER_HEADER      |
			|_________________________|
			|						  |
			|           BODY		  |
			|         (Buffer)		  |
			| size = PacketChunkSize  |
			|						  |
			|_________________________|
			|      BUFFER_HEADER      |
			|_________________________|
			|						  |
			|           BODY		  |
			|         (Buffer)		  |
			| size = PacketChunkSize  |
			|						  |
			|_________________________|
			|						  |
			|						  |
			|						  |
			|						  |
			|			.			  |
			|			.			  |
			|			.			  |
			|						  |
			|						  |
			|						  |
			|						  |
			|_________________________|
			|      BUFFER_HEADER      |
			|_________________________|
			|						  |
			|           BODY		  |
			|         (Buffer)		  |
			| size = PacketChunkSize  |
			|						  |
			|_________________________|

*/

//////////////////////////////////////////////////
//					Functions					//
//////////////////////////////////////////////////


BOOLEAN
LogSendMessageToQueue(UINT32 OperationCode, BOOLEAN IsImmediateMessage, BOOLEAN ShowCurrentSystemTime, const char * Fmt, ...);
