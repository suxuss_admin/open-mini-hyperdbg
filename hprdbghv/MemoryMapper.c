/**
 * @file MemoryMapper.c
 * @author Sina Karvandi (sina@rayanfam.com)
 * @brief This file shows the functions to map memory to reserved system ranges
 * 
 * also some of the functions derived from hvpp
 * - https://github.com/wbenny/hvpp
 * 
 * @version 0.1
 * @date 2020-05-3
 * 
 * @copyright This project is released under the GNU Public License v3.
 * 
 */
#include "pch.h"

 /**
  * @brief 此功能检查页面是否已映射
  * @details 此函数检查页表的PRESENT位，在NMI中，#PF需要手工注入
  *
  * @param Va 虚拟地址
  * @param TargetCr3 目标进程的内核cr3
  * @return BOOLEAN 返回True表示已映射，返回False表示未映射
  */
BOOLEAN
MemoryMapperCheckIfPageIsPresentByCr3(PVOID Va, CR3_TYPE TargetCr3)
{
	PPAGE_ENTRY PageEntry;

	//
	// 查找页表条目
	//
	PageEntry = MemoryMapperGetPteVaByCr3(Va, PT, TargetCr3);

	if (PageEntry->Present)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


/**
 * @brief 获取PMLx上VA的索引
 * 
 * @param Level PMLx
 * @param Va Virtual Address
 * @return UINT64 
 */
UINT64
MemoryMapperGetIndex(PML Level, UINT64 Va)
{
    UINT64 Result = Va;
    Result >>= 12 + Level * 9;

    return Result;
}

/**
 * @brief Get page offset
 * 
 * @param Level PMLx
 * @param Va Virtual Address
 * @return int 
 */
int
MemoryMapperGetOffset(PML Level, UINT64 Va)
{
    UINT64 Result = MemoryMapperGetIndex(Level, Va);
    Result &= (1 << 9) - 1; // 0x1ff

    return Result;
}

/**
 * @brief 此函数获取虚拟地址并返回其虚拟地址的PTE
 * 
 * @param Va Virtual Address
 * @param Level PMLx
 * @return PPAGE_ENTRY virtual address of PTE
 */
PPAGE_ENTRY
MemoryMapperGetPteVa(PVOID Va, PML Level)
{
    CR3_TYPE Cr3;
    UINT64   TempCr3;
    PUINT64  Cr3Va;
    PUINT64  PdptVa;
    PUINT64  PdVa;
    PUINT64  PtVa;
    UINT32   Offset;

    Cr3.Flags = __readcr3();

    //
    // Cr3 should be shifted 12 to the left because it's PFN
    //
    TempCr3 = Cr3.PageFrameNumber << 12;

    //
    // we need VA of Cr3, not PA
    //
    Cr3Va = PhysicalAddressToVirtualAddress(TempCr3);

    Offset = MemoryMapperGetOffset(PML4, Va);

    PPAGE_ENTRY Pml4e = (PPAGE_ENTRY)&Cr3Va[Offset];

    if (!Pml4e->Present || Level == PML4)
    {
        return Pml4e;
    }

    PdptVa = PhysicalAddressToVirtualAddress(Pml4e->PageFrameNumber << 12);
    Offset = MemoryMapperGetOffset(PDPT, Va);

    PPAGE_ENTRY Pdpte = (PPAGE_ENTRY)&PdptVa[Offset];

    if (!Pdpte->Present || Pdpte->LargePage || Level == PDPT)
    {
        return Pdpte;
    }

    PdVa   = PhysicalAddressToVirtualAddress(Pdpte->PageFrameNumber << 12);
    Offset = MemoryMapperGetOffset(PD, Va);

    PPAGE_ENTRY Pde = (PPAGE_ENTRY)&PdVa[Offset];

    if (!Pde->Present || Pde->LargePage || Level == PD)
    {
        return Pde;
    }

    PtVa   = PhysicalAddressToVirtualAddress(Pde->PageFrameNumber << 12);
    Offset = MemoryMapperGetOffset(PT, Va);

    PPAGE_ENTRY Pt = (PPAGE_ENTRY)&PtVa[Offset];

    return Pt;
}

/**
 * @brief 该函数获取虚拟地址并根据特定的cr3返回其虚拟地址的PTE
 * @details 目标Cr3应该是内核cr3，因为我们将使用它来转换内核地址，因此应该映射转换地址的内核功能；因此，请勿将KPTI崩溃用户cr3传递给此功能
 * 
 * @param Va Virtual Address
 * @param Level PMLx
 * @param TargetCr3 kernel cr3 of target process
 * @return PPAGE_ENTRY virtual address of PTE based on cr3
 */
PPAGE_ENTRY
MemoryMapperGetPteVaByCr3(PVOID Va, PML Level, CR3_TYPE TargetCr3)
{
    CR3_TYPE Cr3;
    CR3_TYPE CurrentProcessCr3 = {0};
    UINT64   TempCr3;
    PUINT64  Cr3Va;
    PUINT64  PdptVa;
    PUINT64  PdVa;
    PUINT64  PtVa;
    UINT32   Offset;

    //
    // Switch to new process's memory layout
    // It is because, we're not trying to change the cr3 multiple times
    // so instead of using PhysicalAddressToVirtualAddressByCr3 we use
    // PhysicalAddressToVirtualAddress, but keep in mind that cr3 should
    // be a kernel cr3 (not KPTI user cr3) as the functions to translate
    // physical address to virtual address is not mapped on the user cr3
    //
    CurrentProcessCr3 = SwitchOnAnotherProcessMemoryLayoutByCr3(TargetCr3);

    Cr3.Flags = TargetCr3.Flags;

    //
    // Cr3 should be shifted 12 to the left because it's PFN
    //
    TempCr3 = Cr3.PageFrameNumber << 12;

    //
    // we need VA of Cr3, not PA
    //
    Cr3Va = PhysicalAddressToVirtualAddress(TempCr3);

    Offset = MemoryMapperGetOffset(PML4, Va);

    PPAGE_ENTRY Pml4e = (PPAGE_ENTRY)&Cr3Va[Offset];

    if (!Pml4e->Present || Level == PML4)
    {
        return Pml4e;
    }

    PdptVa = PhysicalAddressToVirtualAddress(Pml4e->PageFrameNumber << 12);
    Offset = MemoryMapperGetOffset(PDPT, Va);

    PPAGE_ENTRY Pdpte = (PPAGE_ENTRY)&PdptVa[Offset];

    if (!Pdpte->Present || Pdpte->LargePage || Level == PDPT)
    {
        return Pdpte;
    }

    PdVa   = PhysicalAddressToVirtualAddress(Pdpte->PageFrameNumber << 12);
    Offset = MemoryMapperGetOffset(PD, Va);

    PPAGE_ENTRY Pde = (PPAGE_ENTRY)&PdVa[Offset];

    if (!Pde->Present || Pde->LargePage || Level == PD)
    {
        return Pde;
    }

    PtVa   = PhysicalAddressToVirtualAddress(Pde->PageFrameNumber << 12);
    Offset = MemoryMapperGetOffset(PT, Va);

    PPAGE_ENTRY Pt = (PPAGE_ENTRY)&PtVa[Offset];

    //
    // Restore the original process
    //
    RestoreToPreviousProcess(CurrentProcessCr3);

    return Pt;
}

/**
 * @brief 此函数从系统范围保留内存（无需物理分配）
 * 
 * @param Size Size of reserving buffers
 * @return PVOID Return the VA of the page 
 */
PVOID
MemoryMapperMapReservedPageRange(SIZE_T Size)
{
    //
    // MmAllocateMappingAddress例程保留指定大小的一系列系统虚拟地址空间。
    //
    return MmAllocateMappingAddress(Size, POOLTAG);
}

/**
 * @brief 此函数用于释放以前从系统范围中分配的内存（不进行物理分配）
 * 
 * @param VirtualAddress Virtual Address
 * @return VOID 
 */
VOID
MemoryMapperUnmapReservedPageRange(PVOID VirtualAddress)
{
    MmFreeMappingAddress(VirtualAddress, POOLTAG);
}

/**
 * @brief 此函数获取虚拟地址并返回其PTE（Pml4e）虚拟地址
 * 
 * @param VirtualAddress Virtual Address
 * @return virtual address of PTE (Pml4e)
 */
PVOID
MemoryMapperGetPte(PVOID VirtualAddress)
{
    return MemoryMapperGetPteVa(VirtualAddress, PT);
}

/**
 * @brief 此函数获取虚拟地址并返回其PTE（Pml4e）虚拟地址
 * based on a specific Cr3
 * 
 * @param VirtualAddress Virtual Address
 * @param TargetCr3 Target process cr3
 * @return virtual address of PTE (Pml4e)
 */
PVOID
MemoryMapperGetPteByCr3(PVOID VirtualAddress, CR3_TYPE TargetCr3)
{
    return MemoryMapperGetPteVaByCr3(VirtualAddress, PT, TargetCr3);
}

/**
 * @brief 这个函数映射一个resreved页（4096），并返回它的虚拟adress和PteAddress中的PTE虚拟地址
 * 
 * @param PteAddress Address of Page Table Entry
 * @return virtual address of mapped (not physically) address
 */
PVOID
MemoryMapperMapPageAndGetPte(PUINT64 PteAddress)
{
    UINT64 Va;
    UINT64 Pte;

    //
    // Reserve the page from system va space
    //
    Va = MemoryMapperMapReservedPageRange(PAGE_SIZE);

    //
    // Get the page's Page Table Entry
    //
    Pte = MemoryMapperGetPte(Va);

    *PteAddress = Pte;

    return Va;
}

/**
 * @brief 初始化内存映射器
 * @details This function should be called in vmx non-root
 * in a IRQL <= APC_LEVEL
 *
 * @return VOID
 */
VOID
MemoryMapperInitialize()
{
    UINT64 TempPte;
    UINT32 ProcessorCount = KeQueryActiveProcessorCount(0);

    //
    // Reserve the address for all cores (read pte and va)
    //
    for (size_t i = 0; i < ProcessorCount; i++)
    {
        g_GuestState[i].MemoryMapper.VirualAddress     = MemoryMapperMapPageAndGetPte(&TempPte);
        g_GuestState[i].MemoryMapper.PteVirtualAddress = TempPte;
    }
}

/**
 * @brief 取消初始化内存映射器
 * @details This function should be called in vmx non-root
 * in a IRQL <= APC_LEVEL
 *
 * @return VOID
 */
VOID
MemoryMapperUninitialize()
{
    UINT32 ProcessorCount = KeQueryActiveProcessorCount(0);

    for (size_t i = 0; i < ProcessorCount; i++)
    {
        //
        // Unmap and free the reserved buffer
        //
        MemoryMapperUnmapReservedPageRange(g_GuestState[i].MemoryMapper.VirualAddress);
        g_GuestState[i].MemoryMapper.VirualAddress     = NULL;
        g_GuestState[i].MemoryMapper.PteVirtualAddress = NULL;
    }
}

/**
 * @brief 通过使用PTE映射缓冲区来安全地读取内存
 * @note 在中断中使用该函数需要考虑缺页问题
 * @return BOOLEAN returns TRUE if it was successfull and FALSE if there was error
 */
BOOLEAN
MemoryMapperReadMemorySafeByPte(PHYSICAL_ADDRESS PaAddressToRead, PVOID BufferToSaveMemory, SIZE_T SizeToRead, UINT64 PteVaAddress, UINT64 MappingVa, BOOLEAN InvalidateVpids)
{
    PVOID       Va = MappingVa;
    PVOID       NewAddress;
    PAGE_ENTRY  PageEntry;
    PPAGE_ENTRY Pte = PteVaAddress;

    //
    // 复制一个，因为修改页属性必须在一条指令内
    //
    PageEntry.Flags = Pte->Flags;

    PageEntry.Present = 1;

    //
    // 通常我们希望每一页都是可写的
    //
    PageEntry.Write = 1;

    //
    // Do not flush this page from the TLB on CR3 switch, by setting the global bit in the PTE.
    //
    PageEntry.Global = 1;

    //
    // 将此PTE的PFN设置为提供的物理地址的PFN
    //
    PageEntry.PageFrameNumber = PaAddressToRead.QuadPart >> 12;

    //
    // 应用条目
    //
    Pte->Flags = PageEntry.Flags;

    //
    // 使缓存失效
    //
    __invlpg(Va);

    //
    // 如果我们在vmx根目录下，也可以从vpids中使其无效
    //
    if (InvalidateVpids)
    {
        // __invvpid_addr(VPID_TAG, Va);
    }

    //
    // 计算地址
    //
    NewAddress = (PVOID)((UINT64)Va + (PAGE_4KB_OFFSET & (PaAddressToRead.QuadPart)));

    //
    // Move the address into the buffer in a safe manner
    //

     memcpy(BufferToSaveMemory, NewAddress, SizeToRead);
    

    //
    // Unmap Address
    //
    Pte->Flags = NULL;

    return TRUE;
}

/**
 * @brief 通过使用PTE映射缓冲区来安全地写入内存
 * 
 * @param SourceVA 源虚拟地址
 * @param PaAddressToWrite 目的地址
 * @param SizeToRead Size
 * @param PteVaAddress 目标虚拟地址的PTE
 * @param MappingVa Mapping Virtual Address
 * @param InvalidateVpids 是否使VPID无效
 * @return BOOLEAN 如果成功则返回TRUE，如果有错误则返回FALSE
 */
BOOLEAN
MemoryMapperWriteMemorySafeByPte(PVOID SourceVA, PHYSICAL_ADDRESS PaAddressToWrite, SIZE_T SizeToRead, UINT64 PteVaAddress, UINT64 MappingVa, BOOLEAN InvalidateVpids)
{
    PVOID       Va = MappingVa;
    PVOID       NewAddress;
    PAGE_ENTRY  PageEntry;
    PPAGE_ENTRY Pte = PteVaAddress;

    //
    // Copy the previous entry into the new entry
    //
    PageEntry.Flags = Pte->Flags;

    PageEntry.Present = 1;

    //
    // Generally we want each page to be writable
    //
    PageEntry.Write = 1;

    //
    // Do not flush this page from the TLB on CR3 switch, by setting the
    // global bit in the PTE.
    //
    PageEntry.Global = 1;

    //
    // Set the PFN of this PTE to that of the provided physical address.
    //
    PageEntry.PageFrameNumber = PaAddressToWrite.QuadPart >> 12;

    //
    // Apply the page entry in a single instruction
    //
    Pte->Flags = PageEntry.Flags;

    //
    // Finally, invalidate the caches for the virtual address.
    //
    __invlpg(Va);

    //
    // Also invalidate it from vpids if we're in vmx root
    //
    if (InvalidateVpids)
    {
        // __invvpid_addr(VPID_TAG, Va);
    }

    //
    // Compute the address
    //
    NewAddress = (PVOID)((UINT64)Va + (PAGE_4KB_OFFSET & (PaAddressToWrite.QuadPart)));

    //
    // Move the address into the buffer in a safe manner
    //
    memcpy(NewAddress, SourceVA, SizeToRead);

    //
    // Unmap Address
    //
    Pte->Flags = NULL;

    return TRUE;
}

/**
 * @brief 通过映射缓冲区安全地读取内存 （它是包装器）
 * 
 * @param VaAddressToRead 要读取的虚拟地址
 * @param BufferToSaveMemory 保存目的地
 * @param SizeToRead Size
 * @return BOOLEAN 如果成功，则返回TRUE；如果失败，则返回FALSE
 */
BOOLEAN
MemoryMapperReadMemorySafe(UINT64 VaAddressToRead, PVOID BufferToSaveMemory, SIZE_T SizeToRead)
{
    ULONG            ProcessorIndex = KeGetCurrentProcessorNumber();
    PHYSICAL_ADDRESS PhysicalAddress;

    //
    // Check to see if PTE and Reserved VA already initialized
    //
    if (g_GuestState[ProcessorIndex].MemoryMapper.VirualAddress == NULL ||
        g_GuestState[ProcessorIndex].MemoryMapper.PteVirtualAddress == NULL)
    {
        //
        // Not initialized
        //
        return FALSE;
    }
    
    PhysicalAddress.QuadPart = VirtualAddressToPhysicalAddress(VaAddressToRead);

	if (PhysicalAddress.QuadPart == 0)
	{
		return FALSE;
	}

    return MemoryMapperReadMemorySafeByPte(
        PhysicalAddress,
        BufferToSaveMemory,
        SizeToRead,
        g_GuestState[ProcessorIndex].MemoryMapper.PteVirtualAddress,
        g_GuestState[ProcessorIndex].MemoryMapper.VirualAddress,
        g_GuestState[ProcessorIndex].IsOnVmxRootMode);
}

/**
 * @brief 通过映射缓冲区写入内存（它是一个包装器）
 *
 * @details this function CAN be called from vmx-root mode
 * 
 * @param Destination Destination Virtual Address
 * @param Source Source Virtual Address
 * @param SizeToRead Size
 * @param TargetProcessCr3 CR3 of target process
 * 
 * @return BOOLEAN returns TRUE if it was successfull and FALSE if there was error
 */
BOOLEAN
MemoryMapperWriteMemorySafe(UINT64 Destination, PVOID Source, SIZE_T SizeToRead, CR3_TYPE TargetProcessCr3)
{
    ULONG            ProcessorIndex = KeGetCurrentProcessorNumber();
    PHYSICAL_ADDRESS PhysicalAddress;

    //
    // Check to see if PTE and Reserved VA already initialized
    //
    if (g_GuestState[ProcessorIndex].MemoryMapper.VirualAddress == NULL ||
        g_GuestState[ProcessorIndex].MemoryMapper.PteVirtualAddress == NULL)
    {
        //
        // Not initialized
        //
        return FALSE;
    }

    if (TargetProcessCr3.Flags == NULL)
    {
        PhysicalAddress.QuadPart = VirtualAddressToPhysicalAddress(Destination);
    }
    else
    {
        PhysicalAddress.QuadPart = VirtualAddressToPhysicalAddressByProcessCr3(Destination, TargetProcessCr3);
    }

    return MemoryMapperWriteMemorySafeByPte(
        Source,
        PhysicalAddress,
        SizeToRead,
        g_GuestState[ProcessorIndex].MemoryMapper.PteVirtualAddress,
        g_GuestState[ProcessorIndex].MemoryMapper.VirualAddress,
        g_GuestState[ProcessorIndex].IsOnVmxRootMode);
}

/**
 * @brief 通过映射缓冲区安全地写入内存（它是包装器）
 *
 * @details 不应从vmx-root模式调用此函数
 * 
 * @param Destination 目标虚拟地址
 * @param Source Source 虚拟地址
 * @param SizeToRead Size
 * @param TargetProcessId 目标进程ID
 * 
 * @return BOOLEAN returns TRUE if it was successfull and FALSE if there was error
 */
BOOLEAN
MemoryMapperWriteMemoryUnsafe(UINT64 Destination, PVOID Source, SIZE_T SizeToRead, UINT32 TargetProcessId)
{
    ULONG            ProcessorIndex = KeGetCurrentProcessorNumber();
    PHYSICAL_ADDRESS PhysicalAddress;

    //
    // 检查PTE和保留VA是否已初始化
    //
    if (g_GuestState[ProcessorIndex].MemoryMapper.VirualAddress == NULL ||
        g_GuestState[ProcessorIndex].MemoryMapper.PteVirtualAddress == NULL)
    {
        //
        // Not initialized
        //
        return FALSE;
    }

    if (TargetProcessId == NULL)
    {
        PhysicalAddress.QuadPart = VirtualAddressToPhysicalAddress(Destination);
    }
    else
    {
        PhysicalAddress.QuadPart = VirtualAddressToPhysicalAddressByProcessId(Destination, TargetProcessId);
    }

    return MemoryMapperWriteMemorySafeByPte(
        Source,
        PhysicalAddress,
        SizeToRead,
        g_GuestState[ProcessorIndex].MemoryMapper.PteVirtualAddress,
        g_GuestState[ProcessorIndex].MemoryMapper.VirualAddress,
        g_GuestState[ProcessorIndex].IsOnVmxRootMode);
}

/**
 * @brief 通过映射缓冲区安全地写入内存（它是包装器）
 * 
 * @param DestinationPa 目标物理地址
 * @param Source 源地址
 * @param SizeToRead Size
 * @param TargetProcessId 目标进程ID
 * 
 * @return BOOLEAN returns TRUE if it was successfull and FALSE if there was error 
 */
BOOLEAN
MemoryMapperWriteMemorySafeByPhysicalAddress(UINT64 DestinationPa, PVOID Source, SIZE_T SizeToRead, UINT32 TargetProcessId)
{
    ULONG            ProcessorIndex = KeGetCurrentProcessorNumber();
    PHYSICAL_ADDRESS PhysicalAddress;
	UNREFERENCED_PARAMETER(TargetProcessId);
    //
    // Check to see if PTE and Reserved VA already initialized
    //
    if (g_GuestState[ProcessorIndex].MemoryMapper.VirualAddress == NULL ||
        g_GuestState[ProcessorIndex].MemoryMapper.PteVirtualAddress == NULL)
    {
        //
        // Not initialized
        //
        return FALSE;
    }

    PhysicalAddress.QuadPart = DestinationPa;

    return MemoryMapperWriteMemorySafeByPte(
        Source,
        PhysicalAddress,
        SizeToRead,
        g_GuestState[ProcessorIndex].MemoryMapper.PteVirtualAddress,
        g_GuestState[ProcessorIndex].MemoryMapper.VirualAddress,
        g_GuestState[ProcessorIndex].IsOnVmxRootMode);
}

/**
 * @brief 在目标用户模式应用程序中保留用户模式地址（未分配）
 * @details this function should be called from vmx non-root mode
 *
 * @param ProcessId Target Process Id
 * @param Commit Whether pass the MEM_COMMIT flag to allocator or not
 * @return Reserved address in the target user mode application
 */
UINT64
MemoryMapperReserveUsermodeAddressInTargetProcess(UINT32 ProcessId, BOOLEAN Commit)
{
    NTSTATUS   Status;
    PVOID      AllocPtr  = NULL;
    SIZE_T     AllocSize = PAGE_SIZE;
    PEPROCESS  SourceProcess;
    KAPC_STATE State = {0};

    if (PsGetCurrentProcessId() != ProcessId)
    {
        //
        // User needs another process memory
        //

        if (PsLookupProcessByProcessId(ProcessId, &SourceProcess) != STATUS_SUCCESS)
        {
            //
            // if the process not found
            //
            return STATUS_UNSUCCESSFUL;
        }
        __try
        {
            KeStackAttachProcess(SourceProcess, &State);
            DbgBreakPoint();

            //
            // Allocate (not allocate, just reserve or reserve and allocate) in memory in target process
            //
            Status = ZwAllocateVirtualMemory(
                NtCurrentProcess(),
                &AllocPtr,
                NULL,
                &AllocSize,
                Commit ? MEM_RESERVE | MEM_COMMIT : MEM_RESERVE,
                PAGE_EXECUTE_READWRITE);

            KeUnstackDetachProcess(&State);
        }
        __except (EXCEPTION_EXECUTE_HANDLER)
        {
            KeUnstackDetachProcess(&State);
            return STATUS_UNSUCCESSFUL;
        }
    }
    else
    {
        //
        // Allocate (not allocate, just reserve) in memory in target process
        //
        Status = ZwAllocateVirtualMemory(
            NtCurrentProcess(),
            &AllocPtr,
            NULL,
            &AllocSize,
            Commit ? MEM_RESERVE | MEM_COMMIT : MEM_RESERVE,
            PAGE_EXECUTE_READWRITE);
    }

    if (!NT_SUCCESS(Status))
    {
        return NULL;
    }

    return AllocPtr;
}

/**
 * @brief 将物理地址映射到PTE
 * @details Find the PTE from MemoryMapperGetPteVaByCr3
 * 
 * @param PhysicalAddress Physical Address
 * @param VirtualAddressPte Virtual Address of PTE
 * @param TargetKernelCr3 Target process cr3
 */
VOID
MemoryMapperMapPhysicalAddressToPte(PHYSICAL_ADDRESS PhysicalAddress, PPAGE_ENTRY VirtualAddressPte, CR3_TYPE TargetKernelCr3)
{
    PAGE_ENTRY PageEntry;
    PAGE_ENTRY PreviousPteEntry;
    CR3_TYPE   CurrentProcessCr3;

    //
    // 切换到目标CR3
    //
    CurrentProcessCr3 = SwitchOnAnotherProcessMemoryLayoutByCr3(TargetKernelCr3);

    //
    // 保存上一个条目
    //
    PreviousPteEntry.Flags = VirtualAddressPte->Flags;

    //
    // 读取前面的条目以修改它
    //
    PageEntry.Flags = VirtualAddressPte->Flags;

    //
    // 确保目标PTE可读、可写、可执行、全局等。
    // 设置该页非缺页状态
    PageEntry.Present = 1;

    //
    // It's not a supervisor page
    //
    PageEntry.Supervisor = 1;

    //
    // 通常我们希望每一页都是可写的
    //
    PageEntry.Write = 1;

    //
    // 通过设置PTE中的全局位，不要从CR3开关上的TLB刷新此页面。
    //
    PageEntry.Global = 1;

    //
    // 提供该PTE的物理地址。
    //
    PageEntry.PageFrameNumber = PhysicalAddress.QuadPart >> 12;

    //
    // 在单个指令中应用页面条目
    //
    VirtualAddressPte->Flags = PageEntry.Flags;

    //
    // 最后，使虚拟地址的缓存失效，这样系统会自动刷新
    //
    __invlpg(PhysicalAddressToVirtualAddress(PhysicalAddress.QuadPart));

    //
    // 恢复之前的进程环境
    //
    RestoreToPreviousProcess(CurrentProcessCr3);
}
