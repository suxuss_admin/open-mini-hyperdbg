/**
 * @file IdtEmulation.c
 * @author Sina Karvandi (sina@rayanfam.com)
 * @brief Handlers of Guest's IDT Emulator 
 * @details
 * @version 0.1
 * @date 2020-06-10
 * 
 * @copyright This project is released under the GNU Public License v3.
 * 
 */
#include "pch.h"


/**
 * @brief Handle Nmi and expection vm-exits
 * 
 * @param InterruptExit vm-exit information for interrupt
 * @param CurrentProcessorIndex index of processor
 * @param GuestRegs guest registers
 * @return VOID 
 */
VOID
IdtEmulationHandleExceptionAndNmi(VMEXIT_INTERRUPT_INFO InterruptExit, UINT32 CurrentProcessorIndex, PGUEST_REGS GuestRegs)
{
    ULONG       ErrorCode = 0;
    ULONG64     GuestRip;

	UNREFERENCED_PARAMETER(GuestRegs);
    //
    // Exception or non-maskable interrupt (NMI). Either:
    //	1: Guest software caused an exception and the bit in the exception bitmap associated with exception's vector was set to 1
    //	2: An NMI was delivered to the logical processor and the "NMI exiting" VM-execution control was 1.
    //
    // VM_EXIT_INTR_INFO shows the exit infromation about event that occured and causes this exit
    // Don't forget to read VM_EXIT_INTR_ERROR_CODE in the case of re-injectiong event
    //

    if (InterruptExit.InterruptionType == INTERRUPT_TYPE_SOFTWARE_EXCEPTION && InterruptExit.Vector == EXCEPTION_VECTOR_BREAKPOINT)
    {
        //
        // Reading guest's RIP
        //
        __vmx_vmread(GUEST_RIP, &GuestRip);

        //
        // notify the user about #BP
        //
        LogInfo("Breakpoint Hit (Process Id : 0x%x) at : %llx \n", PsGetCurrentProcessId(), GuestRip);
        //
        g_GuestState[CurrentProcessorIndex].IncrementRip = FALSE;
		//跳过断点指令，可以实现一个很猥琐的反调试
		//g_GuestState[CurrentProcessorIndex].IncrementRip = TRUE;

		//向客户机注入异常
		EventInjectBreakpoint();
    }
    else if (InterruptExit.InterruptionType == INTERRUPT_TYPE_HARDWARE_EXCEPTION && InterruptExit.Vector == EXCEPTION_VECTOR_UNDEFINED_OPCODE)
    {
		//
		// 处理#UD，检查此异常是否是有意的。
		//
		if (!SyscallHookHandleUD(GuestRegs, CurrentProcessorIndex))
		{
			//
			// 如果发现该#UD是无意的，请向Guest添加#UD中断。
			//
			EventInjectUndefinedOpcode(CurrentProcessorIndex);
		}
    }
	else if (InterruptExit.Vector == EXCEPTION_VECTOR_GENERAL_PROTECTION_FAULT)
	{
		/*
		Interrupt 13—General Protection Exception (#GP) 一般保护性异常
		Indicates that the processor detected one of a class of protection violations called “general-protection violations.”
		The conditions that cause this exception to be generated comprise all the protection violations that do not cause
		other exceptions to be generated (such as, invalid-TSS, segment-not-present, stack-fault, or page-fault exceptions). The
		following conditions cause general-protection exceptions to be generated:
		see Interrupt 13—General Protection Exception (#GP)
		*/
		ULONG32 ErrorCode = 0;
		UINT32 ExitInstrLength;

		// Reading guest's RIP
		__vmx_vmread(GUEST_RIP, &GuestRip);
		//读取异常代码长度
		__vmx_vmread(VM_EXIT_INSTRUCTION_LEN, &ExitInstrLength);
		//注入异常代码
		__vmx_vmwrite(VM_ENTRY_INSTRUCTION_LEN, ExitInstrLength);
		//如果有的话写入错误代码
		if (InterruptExit.ErrorCodeValid)
		{
			/*
			The processor pushes an error code onto the exception handler's stack. If the fault condition was detected while
			loading a segment descriptor, the error code contains a segment selector to or IDT vector number for the
			descriptor; otherwise, the error code is 0. The source of the selector in an error code may be any of the following:
			An operand of the instruction.
			A selector from a gate which is the operand of the instruction.
			A selector from a TSS involved in a task switch.
			IDT vector number.
			*/
			__vmx_vmread(VM_EXIT_INTR_ERROR_CODE, &ErrorCode);

			__vmx_vmwrite(VM_ENTRY_EXCEPTION_ERROR_CODE, ErrorCode);
		}

		//注入错误信息
		__vmx_vmwrite(VM_ENTRY_INTR_INFO, InterruptExit.Flags);

		//继续
		g_GuestState[CurrentProcessorIndex].IncrementRip = FALSE;

	}
    else if (InterruptExit.Vector == EXCEPTION_VECTOR_PAGE_FAULT)
    {
        //
        // #PF is treated differently, we have to deal with cr2 too.
        //
        PAGE_FAULT_ERROR_CODE PageFaultCode = {0};

        __vmx_vmread(VM_EXIT_INTR_ERROR_CODE, &PageFaultCode);

        UINT64 PageFaultAddress = 0;

        __vmx_vmread(EXIT_QUALIFICATION, &PageFaultAddress);

        //
        // Test
        //

        //
         LogInfo("#PF Fault = %016llx, Page Write = 0x%x", PageFaultAddress, PageFaultCode.Fields.Write);
        //

        //
        // Cr2用作页面错误地址
        //
        __writecr2(PageFaultAddress);

        g_GuestState[CurrentProcessorIndex].IncrementRip = FALSE;

        //
        // Re-inject the interrupt/exception
        //
		
        __vmx_vmwrite(VM_ENTRY_INTR_INFO, InterruptExit.Flags);
		
        //
        // re-write error code (if any)
        //
        if (InterruptExit.ErrorCodeValid)
        {
            //
            // Read the error code
            //
            __vmx_vmread(VM_EXIT_INTR_ERROR_CODE, &ErrorCode);

            //
            // Write the error code
            //
            __vmx_vmwrite(VM_ENTRY_EXCEPTION_ERROR_CODE, ErrorCode);
        }

		//EventInjectPageFault(PageFaultAddress,ErrorCode);
    }
    else
    {
        //
        // Test
        //
        LogInfo("Interrupt vector : 0x%x\n", InterruptExit.Vector);
        //

        //
        // Re-inject the interrupt/exception
        //
        __vmx_vmwrite(VM_ENTRY_INTR_INFO, InterruptExit.Flags);

        //
        // re-write error code (if any)
        //
        if (InterruptExit.ErrorCodeValid)
        {
            //
            // Read the error code
            //
            __vmx_vmread(VM_EXIT_INTR_ERROR_CODE, &ErrorCode);

            //
            // Write the error code
            //
            __vmx_vmwrite(VM_ENTRY_EXCEPTION_ERROR_CODE, ErrorCode);
        }
    }
}

/**
 * @brief 外部中断vm-exit处理程序
 * 
 * @param InterruptExit interrupt info from vm-exit
 * @param CurrentProcessorIndex processor index
 * @return BOOLEAN 
 */
BOOLEAN
IdtEmulationHandleExternalInterrupt(VMEXIT_INTERRUPT_INFO InterruptExit, UINT32 CurrentProcessorIndex)
{
    BOOLEAN                Interruptible         = TRUE;
    INTERRUPTIBILITY_STATE InterruptibilityState = {0};
    RFLAGS                 GuestRflags           = {0};
    ULONG                  ErrorCode             = 0;

    //
    // In order to enable External Interrupt Exiting we have to set
    // PIN_BASED_VM_EXECUTION_CONTROLS_EXTERNAL_INTERRUPT in vmx
    // pin-based controls (PIN_BASED_VM_EXEC_CONTROL) and also
    // we should enable VM_EXIT_ACK_INTR_ON_EXIT on vmx vm-exit
    // controls (VM_EXIT_CONTROLS), also this function might not
    // always be successful if the guest is not in the interruptible
    // state so it wait for and interrupt-window exiting to re-inject
    // the interrupt into the guest
    //

    if (InterruptExit.Valid && InterruptExit.InterruptionType == INTERRUPT_TYPE_EXTERNAL_INTERRUPT)
    {
        __vmx_vmread(GUEST_RFLAGS, &GuestRflags);
        __vmx_vmread(GUEST_INTERRUPTIBILITY_INFO, &InterruptibilityState);

        //
        // 如果来宾不可中断（例如：来宾被“ mov ss”或EFLAGS.IF == 0阻止），则无法将外部中断注入来宾。
        //
        Interruptible = GuestRflags.InterruptEnableFlag && !InterruptibilityState.BlockingByMovSs;

        if (Interruptible)
        {
            //
            // Re-inject the interrupt/exception
            //
            __vmx_vmwrite(VM_ENTRY_INTR_INFO, InterruptExit.Flags);

            //
            // re-write error code (if any)
            //
            if (InterruptExit.ErrorCodeValid)
            {
                //
                // Read the error code
                //
                __vmx_vmread(VM_EXIT_INTR_ERROR_CODE, &ErrorCode);

                //
                // Write the error code
                //
                __vmx_vmwrite(VM_ENTRY_EXCEPTION_ERROR_CODE, ErrorCode);
            }
        }
        else
        {
            //
            // 我们无法注入中断，因为来宾的状态不可中断，我们必须在中断窗口打开时将其排队，然后重新注入！
            //
            for (size_t i = 0; i < PENDING_INTERRUPTS_BUFFER_CAPACITY; i++)
            {
                //
                // 找一个空的地方
                //
                if (g_GuestState[CurrentProcessorIndex].PendingExternalInterrupts[i] == NULL)
                {
                    //
                    // 保存以供将来重新注入（退出中断窗口）
                    //
                    g_GuestState[CurrentProcessorIndex].PendingExternalInterrupts[i] = InterruptExit.Flags;
                    break;
                }
            }

            //
            // 启用中断窗口退出.
            //
            HvSetInterruptWindowExiting(TRUE);
        }

        //
        // avoid incrementing rip
        //
        g_GuestState[CurrentProcessorIndex].IncrementRip = FALSE;
    }
    else
    {
        LogError("Why we are here ? It's a vm-exit due to the external\n"
                 "interrupt and its type is not external interrupt? weird!\n");

        return FALSE;
    }

    //
    // Signalize whether the interrupt was handled and re-injected or the
    // guest is not in a state of handling the interrupt and we have to
    // wait for a interrupt windows-exiting
    //
    return Interruptible;
}

/**
 * @brief Handle interrupt-window exitings
 * 
 * @param CurrentProcessorIndex processor index
 * @return VOID 
 */
VOID
IdtEmulationHandleInterruptWindowExiting(UINT32 CurrentProcessorIndex)
{
    VMEXIT_INTERRUPT_INFO InterruptExit = {0};
    ULONG                 ErrorCode     = 0;

    //
    // 查找要注入的未决中断
    //

    for (size_t i = 0; i < PENDING_INTERRUPTS_BUFFER_CAPACITY; i++)
    {
        //
        // 找一个空的地方
        //
        if (g_GuestState[CurrentProcessorIndex].PendingExternalInterrupts[i] != NULL)
        {
            //
            // 保存以重新注入（退出中断窗口）
            //
            InterruptExit.Flags = g_GuestState[CurrentProcessorIndex].PendingExternalInterrupts[i];

            //
            // Free the entry
            //
            g_GuestState[CurrentProcessorIndex].PendingExternalInterrupts[i] = NULL;
            break;
        }
    }

    if (InterruptExit.Flags == 0)
    {
        //
        // 暂无任何状态，让我们禁用中断窗口退出
        //
        HvSetInterruptWindowExiting(FALSE);
    }
    else
    {
        //
        // 重新注入中断/异常
        //
        __vmx_vmwrite(VM_ENTRY_INTR_INFO, InterruptExit.Flags);

        //
        // 改写错误代码（如果有）
        //
        if (InterruptExit.ErrorCodeValid)
        {
            //
            // 读取错误代码
            //
            __vmx_vmread(VM_EXIT_INTR_ERROR_CODE, &ErrorCode);

            //
            // 编写错误代码
            //
            __vmx_vmwrite(VM_ENTRY_EXCEPTION_ERROR_CODE, ErrorCode);
        }
    }

    //
    // avoid incrementing rip
    //
    g_GuestState[CurrentProcessorIndex].IncrementRip = FALSE;
}
