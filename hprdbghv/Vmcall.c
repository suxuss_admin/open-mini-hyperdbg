/**
 * @file Vmcall.c
 * @author Sina Karvandi (sina@rayanfam.com)
 * @brief The main VMCALL and Hypercall handler
 * @details
 * @version 0.1
 * @date 2020-04-11
 * 
 * @copyright This project is released under the GNU Public License v3.
 * 
 */
#include "pch.h"

/**
 * @brief Handle vm-exits of VMCALLs
 * 
 * @param GuestRegs Guest Registers
 * @return NTSTATUS 
 */
NTSTATUS
VmxHandleVmcallVmExit(PGUEST_REGS GuestRegs)
{
    //
    // 检查是我们的VMCALL还是Hyper-V相关的VMCALL
    //48564653
    if (GuestRegs->r12 == 0x48564653)
    {
        //
        // 是自己的则调用自己的Vmcall处理程序
        //
        GuestRegs->rax = VmxVmcallHandler(GuestRegs->rcx, GuestRegs->rdx, GuestRegs->r8, GuestRegs->r9);
    }
    else
    {
		
        HYPERCALL_INPUT_VALUE InputValue = {0};
        InputValue.Value                 = GuestRegs->rcx;

        switch (InputValue.Bitmap.CallCode)
        {
        case HvSwitchVirtualAddressSpace:
        case HvFlushVirtualAddressSpace:
        case HvFlushVirtualAddressList:
        case HvCallFlushVirtualAddressSpaceEx:
        case HvCallFlushVirtualAddressListEx:
        {
            InvvpidAllContexts();
            break;
        }
        case HvCallFlushGuestPhysicalAddressSpace:
        case HvCallFlushGuestPhysicalAddressList:
        {
            InveptSingleContext(g_EptState->EptPointer.Flags);
            break;
        }
        }
        //
        // Let the top-level hypervisor to manage it
        //
        GuestRegs->rax = AsmHypervVmcall(GuestRegs->rcx, GuestRegs->rdx, GuestRegs->r8, GuestRegs->r9);
    }
	return STATUS_SUCCESS;
}

/**
 * @brief Main Vmcall Handler
 * 
 * @param VmcallNumber Request Number
 * @param OptionalParam1 
 * @param OptionalParam2 
 * @param OptionalParam3 
 * @return NTSTATUS 
 */
NTSTATUS
VmxVmcallHandler(UINT64 VmcallNumber,
				UINT64 OptionalParams, 
				UINT64 OptionalNumber,
				UINT64 OptionalParam03)
{
    NTSTATUS VmcallStatus = STATUS_UNSUCCESSFUL;
    BOOLEAN  HookResult   = FALSE;
    BOOLEAN  UnsetExec    = TRUE;
    BOOLEAN  UnsetWrite   = TRUE;
    BOOLEAN  UnsetRead    = TRUE;

	PVM_CALL_OptionalParams pOptionalParams = (PVOID)OptionalParams;

    //
    // Only 32bit of Vmcall is valid, this way we can use the upper 32 bit of the Vmcall
    //
	//取命令字
    switch (VmcallNumber & 0xffffffff)
    {
    //测试自定义VMCALL
    case VMCALL_TEST:
    {
        VmcallStatus = VmcallTest(pOptionalParams->OptionalParams[0], pOptionalParams->OptionalParams[1], pOptionalParams->OptionalParams[2]);
        break;
    }
    case VMCALL_VMXOFF:
    {
        VmxVmxoff();
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
	case VMCALL_CHANGE_PAGE_ATTRIB:
	{
		//
		// Mask is the upper 32 bits to this Vmcall
		// Upper 32 bits of the Vmcall contains the attribute mask
		//

		UINT32 AttributeMask = (UINT32)((VmcallNumber & 0xFFFFFFFF00000000) >> 32);
		/*
		//wtf?这里得到的值不是预期的？
		UnsetRead  = (AttributeMask & PAGE_ATTRIB_READ) ? TRUE : FALSE;
		UnsetWrite = (AttributeMask & PAGE_ATTRIB_WRITE) ? TRUE : FALSE;
		UnsetExec  = (AttributeMask & PAGE_ATTRIB_EXEC) ? TRUE : FALSE;
		*/

		if ((AttributeMask & PAGE_ATTRIB_READ) == 0)
		{
			UnsetRead = FALSE;
		}

		if ((AttributeMask & PAGE_ATTRIB_WRITE) == 0)
		{
			UnsetWrite = FALSE;
		}

		if ((AttributeMask & PAGE_ATTRIB_EXEC) == 0)
		{
			UnsetExec = FALSE;
		}

		CR3_TYPE ProcCr3 = { 0 };
		ProcCr3.Flags = pOptionalParams->OptionalParams[2];
		HookResult = EptHookPerformPageHook2(pOptionalParams->OptionalParams[0] /* TargetAddress */,
			pOptionalParams->OptionalParams[1] /* Hook Function*/,
			ProcCr3 /* Process cr3 */,
			UnsetRead,
			UnsetWrite,
			UnsetExec);

		VmcallStatus = (HookResult == TRUE) ? STATUS_SUCCESS : STATUS_UNSUCCESSFUL;

		break;
	}
    //使ept缓存表中的单个上下文无效
    case VMCALL_INVEPT_SINGLE_CONTEXT:
    {
        InveptSingleContext(pOptionalParams->OptionalParams[0]);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    //使ept缓存表中的所有上下文无效
    case VMCALL_INVEPT_ALL_CONTEXTS:
    {
        InveptAllContexts();
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
	//卸载并恢复所有HOOK
	case VMCALL_UNHOOK_ALL_PAGES:
	{
		EptHookRestoreAllHooksToOrginalEntry();
		VmcallStatus = STATUS_SUCCESS;
		break;
	}
	//卸载并恢复指定HOOK
	case VMCALL_UNHOOK_SINGLE_PAGE:
	{
		if (!EptHookRestoreSingleHookToOrginalEntry(pOptionalParams->OptionalParams[0]))
		{
			VmcallStatus = STATUS_UNSUCCESSFUL;
		}
		break;
	}
	//启动syscallhook
	case VMCALL_ENABLE_SYSCALL_HOOK_EFER:
	{
		SyscallHookConfigureEFER(TRUE);
		break;
	}
	//禁止syscallhook
	case VMCALL_DISABLE_SYSCALL_HOOK_EFER:
	{
		SyscallHookConfigureEFER(FALSE);
		break;
	}
    //更改MSR位图以进行读取
    case VMCALL_CHANGE_MSR_BITMAP_READ:
    {
        HvPerformMsrBitmapReadChange(pOptionalParams->OptionalParams[0]);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    //更改MSR位图进行写入
    case VMCALL_CHANGE_MSR_BITMAP_WRITE:
    {
        HvPerformMsrBitmapWriteChange(pOptionalParams->OptionalParams[0]);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_SET_RDTSC_EXITING:
    {
        HvSetTscVmexit(TRUE);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_SET_RDPMC_EXITING:
    {
        HvSetPmcVmexit(TRUE);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_SET_EXCEPTION_BITMAP:
    {
        HvSetExceptionBitmap(pOptionalParams->OptionalParams[0]);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_ENABLE_MOV_TO_DEBUG_REGS_EXITING:
    {
        HvSetMovDebugRegsExiting(TRUE);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_ENABLE_EXTERNAL_INTERRUPT_EXITING:
    {
        HvSetExternalInterruptExiting(TRUE);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_CHANGE_IO_BITMAP:
    {
        HvPerformIoBitmapChange(pOptionalParams->OptionalParams[0]);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_DISABLE_EXTERNAL_INTERRUPT_EXITING:
    {
        HvSetExternalInterruptExiting(FALSE);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_ENABLE_BREAKPOINT_ON_EXCEPTION_BITMAP:
    {
        //
        // 在断点上启用虚拟机退出（位图除外）
        //
        HvSetExceptionBitmap(EXCEPTION_VECTOR_BREAKPOINT);
        VmcallStatus = STATUS_SUCCESS;

        break;
    }
    case VMCALL_DISABLE_BREAKPOINT_ON_EXCEPTION_BITMAP:
    {
        //
        // 在断点上禁用VM退出（位图除外）
        //
		
        HvUnsetExceptionBitmap(EXCEPTION_VECTOR_BREAKPOINT);
        VmcallStatus = STATUS_SUCCESS;

        break;
    }
    case VMCALL_UNSET_RDTSC_EXITING:
    {
        HvSetTscVmexit(FALSE);
        VmcallStatus = STATUS_SUCCESS;
        break;

        break;
    }
    case VMCALL_UNSET_RDPMC_EXITING:
    {
        HvSetPmcVmexit(FALSE);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_DISABLE_MOV_TO_DEBUG_REGS_EXITING:
    {
        HvSetMovDebugRegsExiting(FALSE);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_RESET_MSR_BITMAP_READ:
    {
        HvPerformMsrBitmapReadReset();
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_RESET_MSR_BITMAP_WRITE:
    {
        HvPerformMsrBitmapWriteReset();
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_RESET_EXCEPTION_BITMAP:
    {
        HvResetExceptionBitmap();
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_RESET_IO_BITMAP:
    {
        HvPerformIoBitmapReset();
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_ENABLE_MOV_TO_CR3_EXITING:
    {
        HvSetMovToCr3Vmexit(TRUE);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
    case VMCALL_DISABLE_MOV_TO_CR3_EXITING:
    {
        HvSetMovToCr3Vmexit(FALSE);
        VmcallStatus = STATUS_SUCCESS;
        break;
    }
	case VMCALL_FLUSH_EPTHOOK:
	{
		HvEptHookFlushAndInvalidateTLB();
		VmcallStatus = STATUS_SUCCESS;
		break;
	}
    default:
    {
        LogError("Unsupported VMCALL");
        VmcallStatus = STATUS_UNSUCCESSFUL;
        break;
    }
    }
    return VmcallStatus;
}

/**
 * @brief Test Vmcall (VMCALL_TEST)
 * 
 * @param Param1 
 * @param Param2 
 * @param Param3 
 * @return NTSTATUS 
 */
NTSTATUS
VmcallTest(UINT64 Param1, UINT64 Param2, UINT64 Param3)
{
   
	//0x22, 0x333, 0x4444
	if ((Param1 == 0x22) && (Param2 == 0x333) && (Param3 == 0x4444))
	{
		LogInfo("VmcallTest Success!\n");
	}
	else
	{
		LogError("VmcallTest Abnormal value @Param1 = 0x%llx , @Param2 = 0x%llx , @Param3 = 0x%llx\n", Param1, Param2, Param3);
		DbgBreakPoint();
	}
    return STATUS_SUCCESS;
}
