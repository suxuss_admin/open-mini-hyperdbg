/**
 * @file EferHook.c
 * @author Sina Karvandi (sina@rayanfam.com)
 *与EFER Syscall挂钩相关的功能的简要实现
 * @details这是通过以下方法演示的方法得出的
 * - https://revers.engineering/syscall-hooking-via-extended-feature-enable-register-efer/
 * 
 * also some of the functions derived from hvpp
 * - https://github.com/wbenny/hvpp
 * 
 * @version 0.1
 * @date 2020-04-10
 * 
 * @copyright This project is released under the GNU Public License v3.
 * 
 */
#include "pch.h"

volatile LONG EferLock[128];

/**
 * @brief 此功能启用或禁用EFER syscall hook
 * @details This function should be called for the first time that we want to enable EFER hook because after calling this function EFER MSR is loaded from GUEST_EFER instead of loading from the regular EFER MSR.
 * bug:某些老版本未启用KVAS缓解的WIN10这里会爆炸
 * @param 启用EFERSyscall挂钩确定我们是要启用syscall挂钩还是禁用syscall挂钩
 * @return VOID 
 */
VOID
SyscallHookConfigureEFER(BOOLEAN EnableEFERSyscallHook)
{
    EFER_MSR           MsrValue;
    IA32_VMX_BASIC_MSR VmxBasicMsr     = {0};
    UINT32             VmEntryControls = 0;
    UINT32             VmExitControls  = 0;
    //
    // Reading IA32_VMX_BASIC_MSR
    //
    VmxBasicMsr.All = __readmsr(MSR_IA32_VMX_BASIC);

    //
    // Read previous VM-Entry and VM-Exit controls
    //
    __vmx_vmread(VM_ENTRY_CONTROLS, &VmEntryControls);
    __vmx_vmread(VM_EXIT_CONTROLS, &VmExitControls);

    MsrValue.Flags = __readmsr(MSR_EFER);

    if (EnableEFERSyscallHook)
    {
        MsrValue.SyscallEnable = FALSE;
        //
        // 设置VM条目控件以加载EFER
		// 2.2.1 Extended Feature Enable Register
        //
        __vmx_vmwrite(VM_ENTRY_CONTROLS, HvAdjustControls(VmEntryControls | VM_ENTRY_LOAD_IA32_EFER, VmxBasicMsr.Fields.VmxCapabilityHint ? MSR_IA32_VMX_TRUE_ENTRY_CTLS : MSR_IA32_VMX_ENTRY_CTLS));

        //
        // 设置VM退出控件以保存EFER
        //
        __vmx_vmwrite(VM_EXIT_CONTROLS, HvAdjustControls(VmExitControls | VM_EXIT_SAVE_IA32_EFER, VmxBasicMsr.Fields.VmxCapabilityHint ? MSR_IA32_VMX_TRUE_EXIT_CTLS : MSR_IA32_VMX_EXIT_CTLS));

        //
        // 设置GUEST_EFER以将此值用作EFER
        //
        __vmx_vmwrite(GUEST_EFER, MsrValue.Flags);

        //
        // 另外，我们必须设置异常位图以在#UDs上导致vm-exit
        //
        HvSetExceptionBitmap(EXCEPTION_VECTOR_UNDEFINED_OPCODE);

        LogInfo("Start EferHook successfully :)\n");
    }
    else
    {

        MsrValue.SyscallEnable = TRUE;

        //
        // Set VM-Entry controls to load EFER
        //
		
        __vmx_vmwrite(VM_ENTRY_CONTROLS, HvAdjustControls(VmEntryControls & ~VM_ENTRY_LOAD_IA32_EFER, VmxBasicMsr.Fields.VmxCapabilityHint ? MSR_IA32_VMX_TRUE_ENTRY_CTLS : MSR_IA32_VMX_ENTRY_CTLS));

        //
        // Set VM-Exit controls to save EFER
        //
	
        __vmx_vmwrite(VM_EXIT_CONTROLS, HvAdjustControls(VmExitControls & ~VM_EXIT_SAVE_IA32_EFER, VmxBasicMsr.Fields.VmxCapabilityHint ? MSR_IA32_VMX_TRUE_EXIT_CTLS : MSR_IA32_VMX_EXIT_CTLS));

		//
		// 设置GUEST_EFER以将此值用作EFER
		//
		__vmx_vmwrite(GUEST_EFER, MsrValue.Flags);

        //载入EFER
        __writemsr(MSR_EFER, MsrValue.Flags);
        //
        // 取消设置异常，以免在#UD上导致vm-exit
        //
        HvUnsetExceptionBitmap(EXCEPTION_VECTOR_UNDEFINED_OPCODE);

        LogInfo("Close EferHook successfully :)\n");
    }
}
/**
 * @brief 此函数模拟SYSCALL执行
 * 
 * @param Regs Guest registers
 * @return BOOLEAN
 */
BOOLEAN
SyscallHookEmulateSYSCALL(PGUEST_REGS Regs)
{
    SEGMENT_SELECTOR Cs, Ss;
    UINT32           InstructionLength;
    UINT64           MsrValue;
    ULONG64          GuestRip;
    ULONG64          GuestRflags;

    //
    // 阅读Guest的RIP
    //
    __vmx_vmread(GUEST_RIP, &GuestRip);

    //
    // 阅读指令长度
    //
    __vmx_vmread(VM_EXIT_INSTRUCTION_LEN, &InstructionLength);

    //
    // 读取 guest's Rflags
    //
    __vmx_vmread(GUEST_RFLAGS, &GuestRflags);

    //
    // 将SYSCALL之后的指令地址保存到RCX，然后从MSR LSTAR加载RIP。
    //
	// MSR_LSTAR指向 KiSystemCall64 
	// CSTAR 指向 KiSystemCall32
    MsrValue  = __readmsr(MSR_LSTAR);
    Regs->rcx = GuestRip + InstructionLength;
    GuestRip  = MsrValue;

	LogInfo("SYSCALL instruction => 0x%llX", GuestRip);
    __vmx_vmwrite(GUEST_RIP, GuestRip);

    //
    // 将RFLAGS保存到R11，然后使用MSR FMASK屏蔽RFLAGS
    //
    MsrValue  = __readmsr(MSR_FMASK);
    Regs->r11 = GuestRflags;
    GuestRflags &= ~(MsrValue | X86_FLAGS_RF);
    __vmx_vmwrite(GUEST_RFLAGS, GuestRflags);

    //
    // Load the CS and SS selectors with values derived from bits 47:32 of MSR_STAR
    //
    MsrValue             = __readmsr(MSR_STAR);
    Cs.SEL               = (UINT16)((MsrValue >> 32) & ~3); // STAR[47:32] & ~RPL3
    Cs.BASE              = 0;                               // flat segment
    Cs.LIMIT             = (UINT32)~0;                      // 4GB limit
    Cs.ATTRIBUTES.UCHARs = 0xA09B;                          // L+DB+P+S+DPL0+Code //A09B
    SetGuestCs(&Cs);

    Ss.SEL               = (UINT16)(((MsrValue >> 32) & ~3) + 8); // STAR[47:32] + 8
    Ss.BASE              = 0;                                     // flat segment
    Ss.LIMIT             = (UINT32)~0;                            // 4GB limit
    Ss.ATTRIBUTES.UCHARs = 0xC093;                                // G+DB+P+S+DPL0+Data //C093
    SetGuestSs(&Ss);

    return TRUE;
}

/**
 * @brief 此函数模拟SYSRET执行
 * 
 * @param Regs Guest registers
 * @return BOOLEAN
 */
BOOLEAN
SyscallHookEmulateSYSRET(PGUEST_REGS Regs)
{
    SEGMENT_SELECTOR Cs, Ss;
    UINT64           MsrValue;
    ULONG64          GuestRip;
    ULONG64          GuestRflags;

    //
    // Load RIP from RCX
    //
    GuestRip = Regs->rcx;
    __vmx_vmwrite(GUEST_RIP, GuestRip);

    //
    // Load RFLAGS from R11. Clear RF, VM, reserved bits
    //
    GuestRflags = (Regs->r11 & ~(X86_FLAGS_RF | X86_FLAGS_VM | X86_FLAGS_RESERVED_BITS)) | X86_FLAGS_FIXED;
    __vmx_vmwrite(GUEST_RFLAGS, GuestRflags);

    //
    // SYSRET loads the CS and SS selectors with values derived from bits 63:48 of MSR_STAR
    //

    MsrValue             = __readmsr(MSR_STAR);
    Cs.SEL               = (UINT16)(((MsrValue >> 48) + 16) | 3); // (STAR[63:48]+16) | 3 (* RPL forced to 3 *)
    Cs.BASE              = 0;                                     // Flat segment
    Cs.LIMIT             = (UINT32)~0;                            // 4GB limit
    Cs.ATTRIBUTES.UCHARs = 0xA0FB;                                // L+DB+P+S+DPL3+Code
    SetGuestCs(&Cs);

	
    Ss.SEL               = (UINT16)(((MsrValue >> 48) + 8) | 3); // (STAR[63:48]+8) | 3 (* RPL forced to 3 *)
    Ss.BASE              = 0;                                    // Flat segment
    Ss.LIMIT             = (UINT32)~0;                           // 4GB limit
    Ss.ATTRIBUTES.UCHARs = 0xC0F3;                               // G+DB+P+S+DPL3+Data
    SetGuestSs(&Ss);
	

    return TRUE;
}

    /**
 * @brief Detect whether the #UD was because of Syscall or Sysret or not
 * 
 * @param Regs Guest register
 * @param CoreIndex Logical core index
 * @return BOOLEAN Shows whther the caller should inject #UD on the guest or not
 */
BOOLEAN
SyscallHookHandleUD(PGUEST_REGS Regs, UINT32 CoreIndex)
{
	CR3_TYPE  GuestCr3;
	CR3_TYPE  OriginalCr3;
    UINT64  Rip;
    BOOLEAN Result;
	UINT32  InstructionLength;
    UINT64    MsrValue;
    //
    // Reading guest's RIP
    //
    __vmx_vmread(GUEST_RIP, &Rip);

    //
    // Due to KVA Shadowing, we need to switch to a different directory table base if the PCID indicates this is a user mode directory table base.
    //

    NT_KPROCESS * CurrentProcess = (NT_KPROCESS *)(PsGetCurrentProcess());
    GuestCr3.Flags                   = CurrentProcess->DirectoryTableBase;
     
    __vmx_vmread(VM_EXIT_INSTRUCTION_LEN, &InstructionLength);
	__vmx_vmread(VM_EXIT_INSTRUCTION_LEN, &InstructionLength);
    //未启用PCID
    if ((GuestCr3.Flags  & PCID_MASK) != PCID_NONE)
    {
        OriginalCr3.Flags = __readcr3();

        __writecr3(GuestCr3.Flags);
	
        //
        // Read the memory
        //
		// 检查指令长度，防炸    【Fix】
		CHAR * InstructionBuffer[3] = { 0 };
		if (InstructionLength == 2)
		{
            if (!MemoryMapperCheckIfPageIsPresentByCr3(Rip, GuestCr3))
            {
                goto ReadPageErr;
            }
            MemoryMapperReadMemorySafe(Rip, InstructionBuffer, 2);

            if (IS_SYSCALL_INSTRUCTION(InstructionBuffer))
			{
				__writecr3(OriginalCr3.Flags);
				goto EmulateSYSCALL;
			}
		}
		else
		if (InstructionLength == 3)
		{
            if (!MemoryMapperCheckIfPageIsPresentByCr3(Rip, GuestCr3))
            {
                goto ReadPageErr;
            }
            MemoryMapperReadMemorySafe(Rip, InstructionBuffer, 3);
            if (IS_SYSRET_INSTRUCTION(InstructionBuffer))
			{
				__writecr3(OriginalCr3.Flags);
				goto EmulateSYSRET;
			}

		}
        
        __writecr3(OriginalCr3.Flags);
        return FALSE;
    }
    else
    {
		if (InstructionLength == 2)
		{
            if (!MemoryMapperCheckIfPageIsPresentByCr3(Rip, GuestCr3))
            {
                goto ReadPageErr;
            }

			OriginalCr3.Flags = __readcr3();

			__writecr3(GuestCr3.Flags);

			if (IS_SYSCALL_INSTRUCTION(Rip))
			{
				__writecr3(OriginalCr3.Flags);
				goto EmulateSYSCALL;
			}
		}
		else
		if (InstructionLength == 3)
		{
            if (!MemoryMapperCheckIfPageIsPresentByCr3(Rip, GuestCr3))
            {
                goto ReadPageErr;
            }

			OriginalCr3.Flags = __readcr3();

			__writecr3(GuestCr3.Flags);

			if (IS_SYSRET_INSTRUCTION(Rip))
			{
				__writecr3(OriginalCr3.Flags);
				goto EmulateSYSRET;
			}
		}

        //if (IS_SYSRET_INSTRUCTION(Rip))
        //    goto EmulateSYSRET;
        //if (IS_SYSCALL_INSTRUCTION(Rip))
        //    goto EmulateSYSCALL;
        return FALSE;
    }

    //----------------------------------------------------------------------------------------

    //
    // Emulate SYSRET instruction
    //
EmulateSYSRET:
    //
    // Test
    //

    //
    // LogInfo("SYSRET instruction => 0x%llX", Rip);
    //

    //
    // We should trigger the event of SYSRET here
    //

    Result                               = SyscallHookEmulateSYSRET(Regs);
    g_GuestState[CoreIndex].IncrementRip = FALSE;

    return Result;
    //
    // Emulate SYSCALL instruction
    //

EmulateSYSCALL:

    //
    // Test
    //

    //
    //LogInfo("SYSCALL instruction => 0x%llX , Process Id : 0x%x", Rip, PsGetCurrentProcessId());
    //
	// eax = SSDT Number
	// Rip = Syscall address
	// 

    //
    // 我们应该在这里触发SYSCALL事件，我们以rax发送syscall号码
    //

  //  Result = SyscallHookEmulateSYSCALL(Regs);
    if (KiSystemCall64Address = NULL)
    {
        KiSystemCall64Address = __readmsr(MSR_LSTAR);
    }
    SyscallHookConfigureEFER(FALSE);
    LogInfo("SYSCALL address => 0x%llX , Process Id : 0x%x", KiSystemCall64Address, PsGetCurrentProcessId());
    g_GuestState[CoreIndex].IncrementRip = FALSE;
    return TRUE;

/*
	实现Rip缺页换页
	感谢wbenny、SinaKarvandi指点
*/
ReadPageErr:
    LogInfo("Read Code PageFault ,Rip = 0x%llX \n", Rip);
	//Rip应该不存在换不到页的情况。
	//向系统注入缺页中断
	EventInjectPageFaultEx(PAGE_ALIGN(Rip));
	//指令重入
	g_GuestState[KeGetCurrentProcessorNumber()].IncrementRip = FALSE;

    //__writecr3(OriginalCr3);
	//返回True，让syscall重入，立即触发缺页
	return TRUE;
}
