PUBLIC AsmVmexitHandler
EXTERN VmxVmexitHandler:PROC
EXTERN VmxVmresume:PROC
EXTERN HvReturnStackPointerForVmxoff:PROC
EXTERN HvReturnInstructionPointerForVmxoff:PROC
.code _text

;------------------------------------------------------------------------
AsmVmexitHandler PROC
    
    push 0  ; 我们可能处于未对齐的堆栈状态，因此堆栈之前的内存可能会导致irql更少或相等，因为它不存在，因此我们只添加一些额外的空间来避免这种错误
	; 我们压入一个0，之后会利用这个0保存esp

    pushfq  ; 8 Byte

    push r15
    push r14
    push r13
    push r12
    push r11
    push r10
    push r9
    push r8        
    push rdi
    push rsi
    push rbp
    push rbp	; rsp
    push rbx
    push rdx
    push rcx
    push rax	

	mov rcx, rsp		; Fast call argument to PGUEST_REGS
	sub	rsp, 28h		; Free some space for Shadow Section
	call	VmxVmexitHandler
	add	rsp, 28h		; Restore the state

	cmp	al, 1	; 检查是否必须关闭VMX (the result is in RAX)
	je		AsmVmxoffHandler

	RestoreState:
	
	pop rax
    pop rcx
    pop rdx
    pop rbx
    pop rbp		; rsp
    pop rbp
    pop rsi
    pop rdi 
    pop r8
    pop r9
    pop r10
    pop r11
    pop r12
    pop r13
    pop r14
    pop r15


    popfq

	sub rsp, 0100h      ; 避免将来的功能出错
	jmp VmxVmresume

AsmVmexitHandler ENDP

;------------------------------------------------------------------------

AsmVmxoffHandler PROC
    sub rsp, 020h       ; shadow space
    call HvReturnStackPointerForVmxoff
    add rsp, 020h       ; remove for shadow space

    mov [rsp+88h], rax  ; 现在，rax包含rsp ，利用开头压入的0将Rsp保存一下【Fix】

    sub rsp, 020h       ; shadow space
    call HvReturnInstructionPointerForVmxoff ; rax中存放着当前客户机eip
    add rsp, 020h       ; remove for shadow space

    mov rdx, rsp        ; save current rsp 保存当前的rsp

    mov rbx, [rsp+88h] ; read rsp again 从堆栈里取回原始rsp【Fix】

    mov rsp, rbx		; 恢复到原始堆栈

    push rax            ; 将eip压入原始堆栈

    mov rsp, rdx        ; 再恢复回来，真尼玛绕
                        
    sub rbx,08h         ; 我们压入了RIP，所以我们必须从上一个堆栈中添加（sub）+8，而且rbx已经包含rsp
    mov [rsp+88h], rbx ; 计算好的新rsp送入堆栈【Fix】

	RestoreState:

	pop rax
    pop rcx
    pop rdx
    pop rbx
    pop rbp		         ; rsp
    pop rbp
    pop rsi
    pop rdi 
    pop r8
    pop r9
    pop r10
    pop r11
    pop r12
    pop r13
    pop r14
    pop r15


    popfq

	pop		rsp     ; 
	ret             ; jump back to where we called Vmcall，之前栈顶被压入了客户机的RIP，所以这里会返回客户机的RIP

AsmVmxoffHandler ENDP

;------------------------------------------------------------------------

END
